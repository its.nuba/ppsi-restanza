package id.dpapayas.restanza.api;

import java.util.List;

import id.dpapayas.restanza.api.model.CreateOrderResponse;
import id.dpapayas.restanza.api.model.DataCreateOrder;
import id.dpapayas.restanza.api.model.FoodTypeResponse;
import id.dpapayas.restanza.api.model.LoginRequest;
import id.dpapayas.restanza.api.model.LoginResponse;
import id.dpapayas.restanza.api.model.MenuResponse;
import id.dpapayas.restanza.api.model.PostOrderRequest;
import id.dpapayas.restanza.api.model.PostOrderResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by dpapayas on 11/7/16.
 */

public interface APIInterface {

    @GET("api/menu/withfoodtype")
    Call<MenuResponse> getAllMenu();

    @GET("api/menu/foodtype")
    Call<FoodTypeResponse> getFoodType();

    @GET("api/menu/withfoodtype/{type}")
    Call<MenuResponse> getAllFood(@Path("type") int type);

    @GET("api/menu/withfoodtype/1")
    Call<MenuResponse> getAllSnack();

    @GET("api/menu/withfoodtype/2")
    Call<MenuResponse> getAllBeverage();

    @POST("api/login")
    Call<LoginResponse> loginTable(@Body LoginRequest loginRequest);

    @POST("api/order")
    Call<PostOrderResponse> postOrder(@Body PostOrderRequest postOrderRequest);

    @POST("api/order/{path}/Detail")
    Call<CreateOrderResponse> CreateOrder(@Body List<DataCreateOrder> createOrders, @Path("path") int type);


//    @POST("oauth/token")
//    Call<LoginResponse> loginUser(@Field("username") String email, @Field("password") String password, @Field("grant_type") String grant_type, @Field("client_id") String client_id, @Field("client_secret") String client_secret);
//
//    @FormUrlEncoded
//    @POST("register")
//    Call<RegisterResponse> regsiter(@Field("username") String email, @Field("password") String password, @Field("name") String name, @Field("phone") String phone, @Field("gender") String gender);
//
//    @GET("user/keinginan")
//    Call<List<Wishlist>> getUserWishlist(@Header("Authorization") String authorization);

}
