package id.dpapayas.restanza;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.dpapayas.restanza.adapter.ItemListTroliAdapter;
import id.dpapayas.restanza.adapter.ItemProgressAdapter;
import id.dpapayas.restanza.realm.RealmController;
import id.dpapayas.restanza.realm.object.DataMenuRealm;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by dpapayas on 5/20/17.
 */

public class DetailProgressActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.listOrder)
    RecyclerView listOrder;
    @BindView(R.id.btnOrderLagi)
    Button btnOrderLagi;
    Realm realm;
    ItemProgressAdapter itemProgressAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_menu_progress);
        ButterKnife.bind(this);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Progress Order");

        listOrder = (RecyclerView) findViewById(R.id.listOrder);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listOrder.setLayoutManager(mLayoutManager);

        realm = RealmController.getInstance().getRealm();
        RealmResults<DataMenuRealm> produkData = RealmController.with(this).getOrderMenu();

        itemProgressAdapter = new ItemProgressAdapter(this, produkData);
        listOrder.setAdapter(itemProgressAdapter);
    }

    @OnClick(R.id.btnOrderLagi)
    public void onViewClicked() {
        Intent intent = new Intent(DetailProgressActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
