package id.dpapayas.restanza.realm;

/**
 * Created by dpapayas on 11/9/16.
 */

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import id.dpapayas.restanza.realm.object.DataMenuRealm;
import io.realm.Realm;
import io.realm.RealmResults;

public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    public RealmResults<DataMenuRealm> getOrderMenu() {

        return realm.where(DataMenuRealm.class).findAll();
    }

    public DataMenuRealm getDataMenu(int id) {

        return realm.where(DataMenuRealm.class).equalTo("id", id).findFirst();
    }

    public void DeleteOrder(final int id) {
        final RealmResults<DataMenuRealm> results = realm.where(DataMenuRealm.class).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                DataMenuRealm troliDataRealm = results.get(id);
                troliDataRealm.deleteFromRealm();
            }
        });

    }


}