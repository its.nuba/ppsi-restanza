package id.dpapayas.restanza.fragment;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.dpapayas.restanza.R;
import id.dpapayas.restanza.adapter.CardViewMenuAdapter;
import id.dpapayas.restanza.api.APIClient;
import id.dpapayas.restanza.api.APIInterface;
import id.dpapayas.restanza.api.model.DataMenu;
import id.dpapayas.restanza.api.model.MenuResponse;
import id.dpapayas.restanza.realm.RealmController;
import id.dpapayas.restanza.realm.object.DataMenuRealm;
import id.dpapayas.restanza.util.RecyclerViewItemClickListener;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dpapayas on 4/22/17.
 */

public class DashboardFragment extends Fragment implements View.OnClickListener {

    List<DataMenu> dataMenuList;
    FragmentPageAdapter mAdapter;

    Unbinder unbinder;

    CardViewMenuAdapter cardViewMenuAdapter;
    @BindView(R.id.relMenu)
    RecyclerView relMenu;
    @BindView(R.id.btnFood)
    Button btnFood;
    @BindView(R.id.btnBeverage)
    Button btnBeverage;
    @BindView(R.id.btnSnack)
    Button btnSnack;
    @BindView(R.id.ivImg)
    ImageView ivImg;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvSub)
    TextView tvSub;
    @BindView(R.id.tvDesc)
    TextView tvDesc;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.btnOrder)
    Button btnOrder;

    Realm realm;

    int id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_dashboard, container, false);

        unbinder = ButterKnife.bind(this, rootView);

        this.realm = RealmController.with(this).getRealm();

        final DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 4);
        relMenu.setLayoutManager(mLayoutManager);
        relMenu.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(1), true));
        relMenu.setItemAnimator(new DefaultItemAnimator());
        relMenu.addOnItemTouchListener(
                new RecyclerViewItemClickListener(getActivity(), relMenu, new RecyclerViewItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        tvName.setText(dataMenuList.get(position).getName());
                        tvDesc.setText(dataMenuList.get(position).getDescription());
                        tvSub.setText(dataMenuList.get(position).getFoodType());
                        tvPrice.setText(kursIndonesia.format(dataMenuList.get(position).getPrice()) + "");
                        Glide.with(getActivity()).load(dataMenuList.get(position).getPicture()).into(ivImg);

                        id = position;
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                })
        );

        GetFood(0);

        return rootView;
    }


    private void CallFragmentHome() {

        Fragment newFragment = new HomeFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, newFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void GetFood(int a) {
        APIInterface apiService =
                APIClient.getClient().create(APIInterface.class);

        Call<MenuResponse> call = apiService.getAllFood(a);
        call.enqueue(new Callback<MenuResponse>() {
            @Override
            public void onResponse(Call<MenuResponse> call, Response<MenuResponse> response) {
                dataMenuList = response.body().getData();
                cardViewMenuAdapter = new CardViewMenuAdapter(getActivity(), dataMenuList);
                relMenu.setAdapter(cardViewMenuAdapter);
//                mAdapter = new FragmentPageAdapter(getFragmentManager(), dataMenuList, getResources());
//                InitializePager();
//                SetPager();
            }

            @Override
            public void onFailure(Call<MenuResponse> call, Throwable t) {
                Log.d("ERROR", t.toString());
            }

        });
    }

//    private void InitializePager() {
//        for (int i = 0; i < mAdapter.getCount(); i++) {
//            ImageButton imgDot = new ImageButton(getActivity());
//            imgDot.setTag(i);
//            imgDot.setImageResource(R.drawable.dot_selector);
//            imgDot.setBackgroundResource(0);
//            imgDot.setPadding(5, 5, 5, 5);
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20, 20);
//            imgDot.setLayoutParams(params);
//            if (i == 0)
//                imgDot.setSelected(true);
//
//            llDots.addView(imgDot);
//        }
//    }
//
//    private void SetPager() {
//        viewPagger.setAdapter(mAdapter);
//        viewPagger.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//
//            @Override
//            public void onPageSelected(int pos) {
//                Log.e("", "Page Selected is ===> " + pos);
//                for (int i = 0; i < mAdapter.getCount(); i++) {
//                    if (i != pos) {
//                        ((ImageView) llDots.findViewWithTag(i)).setSelected(false);
//                    }
//                }
//                ((ImageView) llDots.findViewWithTag(pos)).setSelected(true);
//            }
//
//            @Override
//            public void onPageScrolled(int pos, float arg1, int arg2) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int arg0) {
//
//            }
//        });
//    }

    @Override
    public void onClick(View v) {

    }

    @OnClick({R.id.btnFood, R.id.btnBeverage, R.id.btnSnack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnFood:

                btnBeverage.setBackgroundResource(R.drawable.rounded_button_grey);
                btnSnack.setBackgroundResource(R.drawable.rounded_button_grey);
                btnFood.setBackgroundResource(R.drawable.rounded_button);

                btnFood.setTextColor(Color.parseColor("#FFFFFF"));
                btnSnack.setTextColor(Color.parseColor("#000000"));
                btnBeverage.setTextColor(Color.parseColor("#000000"));

                GetFood(0);

                break;
            case R.id.btnBeverage:

                btnBeverage.setBackgroundResource(R.drawable.rounded_button);
                btnSnack.setBackgroundResource(R.drawable.rounded_button_grey);
                btnFood.setBackgroundResource(R.drawable.rounded_button_grey);

                btnFood.setTextColor(Color.parseColor("#000000"));
                btnSnack.setTextColor(Color.parseColor("#000000"));
                btnBeverage.setTextColor(Color.parseColor("#FFFFFF"));

                GetFood(2);

                break;
            case R.id.btnSnack:

                btnBeverage.setBackgroundResource(R.drawable.rounded_button_grey);
                btnSnack.setBackgroundResource(R.drawable.rounded_button);
                btnFood.setBackgroundResource(R.drawable.rounded_button_grey);

                btnFood.setTextColor(Color.parseColor("#000000"));
                btnSnack.setTextColor(Color.parseColor("#FFFFFF"));
                btnBeverage.setTextColor(Color.parseColor("#000000"));

                GetFood(1);
                break;

        }
    }

    @OnClick(R.id.btnOrder)
    public void onViewClicked() {

        DataMenuRealm troliDataRealm = new DataMenuRealm();

        troliDataRealm.setId(dataMenuList.get(id).getId());
        troliDataRealm.setPrice(dataMenuList.get(id).getPrice());
        troliDataRealm.setPicture(dataMenuList.get(id).getPicture());
        troliDataRealm.setName(dataMenuList.get(id).getName());
        troliDataRealm.setActive(dataMenuList.get(id).getIsActive());
        troliDataRealm.setQuantity(1);


        realm.beginTransaction();

        realm.copyToRealm(troliDataRealm);
        realm.commitTransaction();

        PreventDialog();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void PreventDialog(){

        final MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(getActivity());
        materialDialog.title("Informasi");
        materialDialog.content("Order ada telah dimasukkan");
        final MaterialDialog dialog = materialDialog.build();
        dialog.show();

        Handler handler = null;
        handler = new Handler();
        handler.postDelayed(new Runnable(){
            public void run(){
                dialog.dismiss();
            }
        }, 2000);

    }
}
