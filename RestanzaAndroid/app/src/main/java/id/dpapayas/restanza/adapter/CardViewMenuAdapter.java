package id.dpapayas.restanza.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import id.dpapayas.restanza.R;
import id.dpapayas.restanza.api.model.DataMenu;

/**
 * Created by dpapayas on 11/7/16.
 */

public class CardViewMenuAdapter extends RecyclerView.Adapter<CardViewMenuAdapter.MyViewHolder> {

    private Context mContext;
    private List<DataMenu> kategoriDataList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnail;
        public TextView tvName, tvPrice;

        public MyViewHolder(View view) {
            super(view);
            thumbnail = (ImageView) view.findViewById(R.id.iconId);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvPrice = (TextView) view.findViewById(R.id.tvPrice);
        }
    }


    public CardViewMenuAdapter(Context mContext, List<DataMenu> kategoriDataList) {
        this.mContext = mContext;
        this.kategoriDataList = kategoriDataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_menu, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        DataMenu kategori = kategoriDataList.get(position);

        holder.tvName.setText(kategori.getName());
        holder.tvPrice.setText(String.valueOf(kategori.getPrice()));
        Glide.with(mContext).load(kategori.getPicture()).into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return kategoriDataList.size();
    }
}
