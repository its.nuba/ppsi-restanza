package id.dpapayas.restanza.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.HashMap;

import id.dpapayas.restanza.R;
import id.dpapayas.restanza.adapter.CardView2Adapter;
import id.dpapayas.restanza.adapter.CardViewAdapter;
import id.dpapayas.restanza.util.FeedProperties;
import id.dpapayas.restanza.util.SliderLayout;

public class HomeFragment extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    private SliderLayout mDemoSlider;
    private RecyclerView recyclerView, recyclerView2, recyclerView3;
    private CardView cardView;

    private ArrayList<FeedProperties> os_versions, os_versions2, os_versions3;

    private CardViewAdapter mAdapter, mAdapter3;
    private CardView2Adapter mAdapter2;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        recyclerView2 = (RecyclerView) view.findViewById(R.id.my_recycler_view2);
        recyclerView3 = (RecyclerView) view.findViewById(R.id.my_recycler_view3);

        final String[] versions = {"Cheescake", "Coffee", "Drink", "Food",
                "Food Image"};
        final int[] icons = {R.drawable.cheesecake, R.drawable.cofee, R.drawable.drink, R.drawable.food, R.drawable.food_image};

        final String[] versions2 = {"Cheescake", "Coffee"};
        final int[] icons2 = {R.drawable.cheesecake, R.drawable.cofee};


        final String[] versions3 = {"Chesscake", "Sandwhich",
                "Coffee"};
        final int[] icons3 = {R.drawable.cheesecake, R.drawable.sandwich, R.drawable.cofee};


        os_versions = new ArrayList<FeedProperties>();
        os_versions2 = new ArrayList<FeedProperties>();
        os_versions3 = new ArrayList<FeedProperties>();

        for (int i = 0; i < versions.length; i++) {
            FeedProperties feed = new FeedProperties();

            feed.setTitle(versions[i]);
            feed.setThumbnail(icons[i]);
            os_versions.add(feed);
        }

        for (int i = 0; i < versions2.length; i++) {
            FeedProperties feeds = new FeedProperties();

            feeds.setTitle(versions2[i]);
            feeds.setThumbnail(icons2[i]);
            os_versions2.add(feeds);
        }

        Log.d("1", os_versions.size()+"");
        Log.d("2", os_versions2.size()+"");

        for (int i = 0; i < versions3.length; i++) {
            FeedProperties feed = new FeedProperties();

            feed.setTitle(versions3[i]);
            feed.setThumbnail(icons3[i]);
            os_versions3.add(feed);
        }

        recyclerView.setHasFixedSize(true);

        // ListView
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // create an Object for Adapter
        mAdapter = new CardViewAdapter(os_versions);

        // set the adapter object to the Recyclerview
        recyclerView.setAdapter(mAdapter);





        recyclerView2.setHasFixedSize(true);

        // ListView
        recyclerView2.setLayoutManager(new LinearLayoutManager(getActivity()));

        // create an Object for Adapter
        mAdapter2 = new CardView2Adapter(os_versions2);

        // set the adapter object to the Recyclerview
        recyclerView2.setAdapter(mAdapter2);



//        recyclerView3.setHasFixedSize(true);
//
//        // ListView
//        recyclerView3.setLayoutManager(new LinearLayoutManager(getActivity()));
//
//        // create an Object for Adapter
//        mAdapter3 = new CardViewAdapter(os_versions3);
//
//        // set the adapter object to the Recyclerview
//        recyclerView3.setAdapter(mAdapter3);




        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            linearLayoutManager.setOrientation(LinearLayout.HORIZONTAL);
        } else {
            linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        }

        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getActivity());
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            linearLayoutManager2.setOrientation(LinearLayout.HORIZONTAL);
        } else {
            linearLayoutManager2.setOrientation(LinearLayout.VERTICAL);
        }

        LinearLayoutManager linearLayoutManager3 = new LinearLayoutManager(getActivity());
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            linearLayoutManager3.setOrientation(LinearLayout.HORIZONTAL);
        } else {
            linearLayoutManager3.setOrientation(LinearLayout.VERTICAL);
        }



        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView2.setLayoutManager(linearLayoutManager2);
        recyclerView2.setItemAnimator(new DefaultItemAnimator());

//        recyclerView3.setLayoutManager(linearLayoutManager3);
//        recyclerView3.setItemAnimator(new DefaultItemAnimator());


        String[] colors = getResources().getStringArray(R.array.colorList);

        mDemoSlider = (SliderLayout) view.findViewById(R.id.slider);

        HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("Favorite 1", R.drawable.cofee);
        file_maps.put("Favorite 2", R.drawable.sandwich);
        file_maps.put("Favorite 3", R.drawable.cheesecake);
        file_maps.put("Favorite 4", R.drawable.drink);

        for (String name : file_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);


            textSliderView.bundle(new Bundle());

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);


        return view;
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

}

