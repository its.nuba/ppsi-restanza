package id.dpapayas.restanza.util;

import java.util.HashMap;

/**
 * Created by dpapayas on 4/30/17.
 */

public class TotalValTroliEvent {

    private HashMap<Integer, Long> totalValue;

    public HashMap<Integer, Long> getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(HashMap<Integer, Long> totalValue) {
        this.totalValue = totalValue;
    }

    public TotalValTroliEvent(HashMap<Integer, Long> totalValue) {
        this.totalValue = totalValue;
    }
}
