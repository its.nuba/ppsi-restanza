package id.dpapayas.restanza.api.model;

/**
 * Created by dpapayas on 5/20/17.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataPostOrder {

    @SerializedName("tableId")
    @Expose
    private Integer tableId;
    @SerializedName("totalAmount")
    @Expose
    private Integer totalAmount;
    @SerializedName("checkInOn")
    @Expose
    private String checkInOn;
    @SerializedName("checkOutOn")
    @Expose
    private Object checkOutOn;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("table")
    @Expose
    private Object table;
    @SerializedName("orderDetail")
    @Expose
    private List<Object> orderDetail = null;
    @SerializedName("id")
    @Expose
    private Integer id;

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCheckInOn() {
        return checkInOn;
    }

    public void setCheckInOn(String checkInOn) {
        this.checkInOn = checkInOn;
    }

    public Object getCheckOutOn() {
        return checkOutOn;
    }

    public void setCheckOutOn(Object checkOutOn) {
        this.checkOutOn = checkOutOn;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getTable() {
        return table;
    }

    public void setTable(Object table) {
        this.table = table;
    }

    public List<Object> getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(List<Object> orderDetail) {
        this.orderDetail = orderDetail;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}