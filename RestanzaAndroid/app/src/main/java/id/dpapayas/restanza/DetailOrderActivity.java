package id.dpapayas.restanza;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.dpapayas.restanza.adapter.ItemListTroliAdapter;
import id.dpapayas.restanza.api.APIClient;
import id.dpapayas.restanza.api.APIInterface;
import id.dpapayas.restanza.api.model.CreateOrderResponse;
import id.dpapayas.restanza.api.model.DataCreateOrder;
import id.dpapayas.restanza.api.model.LoginRequest;
import id.dpapayas.restanza.api.model.LoginResponse;
import id.dpapayas.restanza.api.model.PostOrderRequest;
import id.dpapayas.restanza.api.model.PostOrderResponse;
import id.dpapayas.restanza.realm.RealmController;
import id.dpapayas.restanza.realm.object.DataMenuRealm;
import id.dpapayas.restanza.util.SessionManager;
import id.dpapayas.restanza.util.TotalValTroliEvent;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dpapayas on 5/19/17.
 */

public class DetailOrderActivity extends AppCompatActivity {

    Realm realm;
    Toolbar toolbar;
    ItemListTroliAdapter itemListTroliAdapter;
    RecyclerView recyclerView;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.listOrder)
    RecyclerView listOrder;
    @BindView(R.id.relBgTotal)
    RelativeLayout relBgTotal;
    @BindView(R.id.tvSubtotal)
    TextView tvSubtotal;
    @BindView(R.id.tvDiscount)
    TextView tvDiscount;
    @BindView(R.id.tvTotal)
    TextView tvTotal;
    @BindView(R.id.btnOrder)
    Button btnOrder;
    private EventBus eventBus = EventBus.getDefault();
    long totalValue;

    String tableId;
    RealmResults<DataMenuRealm> produkData;

    List<DataCreateOrder> dataCreateOrderList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_detail_order);
        ButterKnife.bind(this);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Detail Order");

        SessionManager sessionManager = new SessionManager(getApplicationContext());
//        tableId = sessionManager.getKeyTableid();
        tableId = "4";

        recyclerView = (RecyclerView) findViewById(R.id.listOrder);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        getAllTroli();
    }

    public void getAllTroli() {

        realm = RealmController.getInstance().getRealm();
        produkData = RealmController.with(this).getOrderMenu();
        if (produkData.size() == 0) {
            new MaterialDialog.Builder(this)
                    .title("Informasi")
                    .content("Order anda kosong, saatnya untuk belanja.")
                    .positiveText("Tutup")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {


                        }
                    })
                    .show();

        } else {
            itemListTroliAdapter = new ItemListTroliAdapter(this, produkData, eventBus);
            recyclerView.setAdapter(itemListTroliAdapter);
        }
    }

    @Subscribe
    public void onEventMainThread(final TotalValTroliEvent event) {

        totalValue = 0;
        for (int i = 0; i < event.getTotalValue().size(); i++) {
            totalValue += event.getTotalValue().get(i).longValue();
        }

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        double tax = 0.10 * totalValue;

        double totalNilai = totalValue + tax;

        tvSubtotal.setText(kursIndonesia.format(totalValue));
        tvDiscount.setText(kursIndonesia.format(tax));
        tvTotal.setText(kursIndonesia.format(totalNilai));
    }

    @OnClick(R.id.btnOrder)
    public void onViewClicked() {
        PreventDialog();

        PostOrder(tableId);

        Intent intent = new Intent(DetailOrderActivity.this, DetailProgressActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void PreventDialog() {

        final MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(this);
        materialDialog.title("Informasi");
        materialDialog.content("Order ada telah dipesan, tunggu beberapa saat lagi");
        final MaterialDialog dialog = materialDialog.build();
        dialog.show();

        Handler handler = null;
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                dialog.dismiss();
            }
        }, 2000);

    }

    public void PostOrder(final String tableId) {

        APIInterface apiService =
                APIClient.getClient().create(APIInterface.class);

        PostOrderRequest postOrderRequest = new PostOrderRequest();
        postOrderRequest.setTableid(tableId);
        postOrderRequest.setStatus("1");

        Call<PostOrderResponse> call = apiService.postOrder(postOrderRequest);
        call.enqueue(new Callback<PostOrderResponse>() {
            @Override
            public void onResponse(Call<PostOrderResponse> call, Response<PostOrderResponse> response) {

                String orderId = String.valueOf(response.body().getData().getId());
                CreateOrder(dataCreateOrderList, tableId, orderId);
            }

            @Override
            public void onFailure(Call<PostOrderResponse> call, Throwable t) {
                Log.e("ERROR", t.toString());
            }

        });
    }

    public void CreateOrder(List<DataCreateOrder> dataCreateOrderList, String tableId, String orderId) {

        APIInterface apiService =
                APIClient.getClient().create(APIInterface.class);

        for (int i = 0; i < produkData.size(); i++){
            DataCreateOrder dataCreateOrder = new DataCreateOrder();
            dataCreateOrder.setMenuid(String.valueOf(produkData.get(i).getId()));
            dataCreateOrder.setNote("");
            dataCreateOrder.setCreatedby("2");
            dataCreateOrder.setTotalquantity("1");
            dataCreateOrder.setOrderid(orderId);

            dataCreateOrderList.add(dataCreateOrder);
        }

        Call<CreateOrderResponse> call = apiService.CreateOrder(dataCreateOrderList, Integer.parseInt(orderId));
        call.enqueue(new Callback<CreateOrderResponse>() {
            @Override
            public void onResponse(Call<CreateOrderResponse> call, Response<CreateOrderResponse> response) {
                Log.e("ERROR", response.body().toString());
            }

            @Override
            public void onFailure(Call<CreateOrderResponse> call, Throwable t) {
                Log.e("ERROR", t.toString());
            }

        });
    }

}
