package id.dpapayas.restanza.api.model;

/**
 * Created by dpapayas on 5/18/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataMenu {

    @SerializedName("foodType")
    @Expose
    private String foodType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("isRecomended")
    @Expose
    private Boolean isRecomended;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;
    @SerializedName("updateBy")
    @Expose
    private Object updateBy;
    @SerializedName("updateOn")
    @Expose
    private Object updateOn;
    @SerializedName("createdBy")
    @Expose
    private Integer createdBy;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("id")
    @Expose
    private Integer id;

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Boolean getIsRecomended() {
        return isRecomended;
    }

    public void setIsRecomended(Boolean isRecomended) {
        this.isRecomended = isRecomended;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Object getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Object updateBy) {
        this.updateBy = updateBy;
    }

    public Object getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(Object updateOn) {
        this.updateOn = updateOn;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}