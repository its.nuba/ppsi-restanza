package id.dpapayas.restanza;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dpapayas on 4/20/17.
 */

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.imgMenu1)
    ImageView imgMenu1;
    @BindView(R.id.layMenu1)
    RelativeLayout layMenu1;
    @BindView(R.id.imgMenu2)
    ImageView imgMenu2;
    @BindView(R.id.layMenu2)
    RelativeLayout layMenu2;
    @BindView(R.id.imgMenu3)
    ImageView imgMenu3;
    @BindView(R.id.layMenu3)
    RelativeLayout layMenu3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_home);
        ButterKnife.bind(this);


    }

    @OnClick({R.id.layMenu1, R.id.layMenu2, R.id.layMenu3})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layMenu1:

                imgMenu1.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            imgMenu1.setColorFilter(Color.argb(200, 71, 194, 203));
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    imgMenu1.clearColorFilter();
                                }
                            }, 500);
                        }
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            imgMenu1.setColorFilter(Color.argb(0, 0, 0, 0));
                        }
                        return false;
                    }
                });

                break;
            case R.id.layMenu2:

                imgMenu2.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            imgMenu2.setColorFilter(Color.argb(200, 71, 194, 203));
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    imgMenu2.clearColorFilter();
                                }
                            }, 500);
                        }
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            imgMenu2.setColorFilter(Color.argb(0, 0, 0, 0));
                        }
                        return false;
                    }
                });

                break;
            case R.id.layMenu3:

                imgMenu3.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            imgMenu3.setColorFilter(Color.argb(200, 71, 194, 203));
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    imgMenu3.clearColorFilter();
                                }
                            }, 500);
                        }
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            imgMenu3.setColorFilter(Color.argb(0, 0, 0, 0));
                        }
                        return false;
                    }
                });

                break;
        }
    }
}
