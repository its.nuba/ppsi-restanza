package id.dpapayas.restanza;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import id.dpapayas.restanza.api.APIClient;
import id.dpapayas.restanza.api.APIInterface;
import id.dpapayas.restanza.api.model.LoginRequest;
import id.dpapayas.restanza.api.model.LoginResponse;
import id.dpapayas.restanza.util.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dpapayas on 11/27/16.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    EditText etEmail, etPassword;
    Button btnLogin;
    SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);

        sessionManager = new SessionManager(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);


    }

    public void Login(final String username, String password) {

        APIInterface apiService =
                APIClient.getClient().create(APIInterface.class);

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(username);
        loginRequest.setPassword(password);

        Call<LoginResponse> call = apiService.loginTable(loginRequest);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                sessionManager.spLogin(username, String.valueOf(response.body().getData().getTableId()));
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("ERROR", t.toString());
            }

        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:

                Login(etEmail.getText().toString(), etPassword.getText().toString());

                break;
        }
    }
}
