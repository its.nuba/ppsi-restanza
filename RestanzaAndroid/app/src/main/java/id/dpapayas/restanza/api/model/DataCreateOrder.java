package id.dpapayas.restanza.api.model;

/**
 * Created by dpapayas on 5/20/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataCreateOrder {

    @SerializedName("orderid")
    @Expose
    private String orderid;
    @SerializedName("createdby")
    @Expose
    private String createdby;
    @SerializedName("menuid")
    @Expose
    private String menuid;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("totalquantity")
    @Expose
    private String totalquantity;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTotalquantity() {
        return totalquantity;
    }

    public void setTotalquantity(String totalquantity) {
        this.totalquantity = totalquantity;
    }

}