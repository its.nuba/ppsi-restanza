package id.dpapayas.restanza.util;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Created by dpapayas on 12/9/16.
 */

public class SessionManager {

    SharedPreferences pref;

    Editor editor;

    Context _context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "Restanza";

    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String KEY_NAME = "name";

    public static final String KEY_EMAIL = "email";
    public static final String KEY_ACCESS_TOKEN = "access_token";

    public static final String KEY_TABLEID = "table_id";

    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void spLogin(String name, String tableId){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_TABLEID, tableId);

        editor.commit();
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

    public String getAccessToken(){
        return pref.getString(KEY_ACCESS_TOKEN, "");
    }

    public void deleteSP(){
        pref.edit().clear().commit();

    }

    public String getKeyTableid() {
        return KEY_TABLEID;
    }

    public String getKeyName() {
        return pref.getString(KEY_NAME, "");
    }
}