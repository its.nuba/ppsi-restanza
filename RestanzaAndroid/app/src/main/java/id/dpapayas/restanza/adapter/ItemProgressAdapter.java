package id.dpapayas.restanza.adapter;

/**
 * Created by dpapayas on 5/20/17.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.dpapayas.restanza.R;
import id.dpapayas.restanza.realm.object.DataMenuRealm;


public class ItemProgressAdapter extends RecyclerView.Adapter<ItemProgressAdapter.MyViewHolder> {

    private Context mContext;
    private List<DataMenuRealm> produkDataList;
    List<String> listSpinner = new ArrayList<>();
    Fragment fragment;
    long hasil;
    EventBus eventBus;


    HashMap<Integer, Long> intString = new HashMap<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvStatus;
        public ImageView thumbnail, btnDelete;
        public Spinner spKuantiti;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvStatus = (TextView) view.findViewById(R.id.tvStatus);
            thumbnail = (ImageView) view.findViewById(R.id.imgMenu);
        }
    }


    public ItemProgressAdapter(Context mContext, List<DataMenuRealm> produkDataList) {
        this.mContext = mContext;
        this.produkDataList = produkDataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order_progress, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DataMenuRealm produkData = produkDataList.get(position);

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);


        holder.tvName.setText(produkData.getName());

        String status = "";
        if (produkData.getActive()){
            holder.tvStatus.setTextColor(Color.parseColor("#e74c3c"));
            status = "Ordering";
        } else {
            status = "Order Selesai";
            holder.tvStatus.setTextColor(Color.parseColor("#2ecc71"));
        }
        holder.tvStatus.setText(status);

        Glide.with(mContext).load(produkData.getPicture()).into(holder.thumbnail);


    }

    @Override
    public int getItemCount() {
        return produkDataList.size();
    }

}
