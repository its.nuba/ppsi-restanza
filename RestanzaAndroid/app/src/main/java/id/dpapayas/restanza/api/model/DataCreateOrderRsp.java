package id.dpapayas.restanza.api.model;

/**
 * Created by dpapayas on 5/20/17.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataCreateOrderRsp {

    @SerializedName("lastStatus")
    @Expose
    private Integer lastStatus;
    @SerializedName("lastStatusText")
    @Expose
    private String lastStatusText;
    @SerializedName("lastStatusTime")
    @Expose
    private String lastStatusTime;
    @SerializedName("menu")
    @Expose
    private String menu;
    @SerializedName("menuPrice")
    @Expose
    private Integer menuPrice;
    @SerializedName("tableId")
    @Expose
    private Integer tableId;
    @SerializedName("tableName")
    @Expose
    private String tableName;
    @SerializedName("orderId")
    @Expose
    private Integer orderId;
    @SerializedName("menuId")
    @Expose
    private Integer menuId;
    @SerializedName("totalQuantity")
    @Expose
    private Integer totalQuantity;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("order")
    @Expose
    private Object order;
    @SerializedName("orderDetailStatus")
    @Expose
    private List<Object> orderDetailStatus = null;
    @SerializedName("updateBy")
    @Expose
    private Object updateBy;
    @SerializedName("updateOn")
    @Expose
    private Object updateOn;
    @SerializedName("createdBy")
    @Expose
    private Integer createdBy;
    @SerializedName("createdOn")
    @Expose
    private String createdOn;
    @SerializedName("id")
    @Expose
    private Integer id;

    public Integer getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(Integer lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getLastStatusText() {
        return lastStatusText;
    }

    public void setLastStatusText(String lastStatusText) {
        this.lastStatusText = lastStatusText;
    }

    public String getLastStatusTime() {
        return lastStatusTime;
    }

    public void setLastStatusTime(String lastStatusTime) {
        this.lastStatusTime = lastStatusTime;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public Integer getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(Integer menuPrice) {
        this.menuPrice = menuPrice;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Object getOrder() {
        return order;
    }

    public void setOrder(Object order) {
        this.order = order;
    }

    public List<Object> getOrderDetailStatus() {
        return orderDetailStatus;
    }

    public void setOrderDetailStatus(List<Object> orderDetailStatus) {
        this.orderDetailStatus = orderDetailStatus;
    }

    public Object getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Object updateBy) {
        this.updateBy = updateBy;
    }

    public Object getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(Object updateOn) {
        this.updateOn = updateOn;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}