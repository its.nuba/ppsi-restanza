package id.dpapayas.restanza.fragment;

import java.util.ArrayList;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.GridView;

import id.dpapayas.restanza.R;
import id.dpapayas.restanza.api.model.DataMenu;

public class GridFragment extends Fragment
{
	int mNum;
	int mFirstImage = 0;
	int mImageCount = -1;
	ArrayList<DataMenu> mTopicList;

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		mNum = ((args != null) ? args.getInt("num") : 0);

		if (args != null)
		{
			mTopicList = (ArrayList<DataMenu>) args
					.getSerializable("topicList");
			mFirstImage = args.getInt("firstImage");
		}
		int numRows = getResources().getInteger(R.integer.num_rows);
		int numCols = getResources().getInteger(R.integer.num_cols);
		int numTopicsPerPage = numRows * numCols;
		mImageCount = numTopicsPerPage;

		mFirstImage = (mFirstImage / numTopicsPerPage) * numTopicsPerPage;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		View rootView = getView();
		final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.gridView);
		
//		ViewTreeObserver treeObserver = gridview.getViewTreeObserver();
//		treeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener()
//		{
//			@SuppressWarnings("deprecation")
//			@Override
//			public void onGlobalLayout()
//			{
//				gridview.setColumnWidth(gridview.getWidth() / gridview.getNumColumns());
//				gridview.setAdapter(new GridImageAdapter(getActivity(), mTopicList, mFirstImage, mImageCount, gridview.getHeight()));
//	    		gridview.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//			}
//		});

		RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 4);
		recyclerView.setLayoutManager(mLayoutManager);
		recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(1), true));
		recyclerView.setItemAnimator(new DefaultItemAnimator());
	}

	/**
	 * onCreateView Build the view that shows the grid.
	 */

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		// Build the view that shows the grid.
		View view = inflater.inflate(R.layout.gridview, container, false);
		return view;
	}

	public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

		private int spanCount;
		private int spacing;
		private boolean includeEdge;

		public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
			this.spanCount = spanCount;
			this.spacing = spacing;
			this.includeEdge = includeEdge;
		}

		@Override
		public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
			int position = parent.getChildAdapterPosition(view); // item position
			int column = position % spanCount; // item column

			if (includeEdge) {
				outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
				outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

				if (position < spanCount) { // top edge
					outRect.top = spacing;
				}
				outRect.bottom = spacing; // item bottom
			} else {
				outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
				outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
				if (position >= spanCount) {
					outRect.top = spacing; // item top
				}
			}
		}
	}

	/**
	 * Converting dp to pixel
	 */
	private int dpToPx(int dp) {
		Resources r = getResources();
		return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
	}
}
