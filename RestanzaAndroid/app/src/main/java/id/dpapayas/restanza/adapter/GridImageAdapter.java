package id.dpapayas.restanza.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import id.dpapayas.restanza.R;
import id.dpapayas.restanza.api.model.DataMenu;

public class GridImageAdapter extends RecyclerView.Adapter<GridImageAdapter.ViewHolder> {
    private Context mContext;
    private int mImageOffset = 0; // the index offset into the list of images
    private int mImageCount = -1; // -1 means that we display all images
    private int mNumTopics = 0;
    private ArrayList<DataMenu> mArrayList;

    private int rowHeight = 0;

    public GridImageAdapter(Activity activity, ArrayList<DataMenu> list, int imageOffset, int imageCount, int rowHeight) {
        mArrayList = list;
        mContext = activity;
        mImageOffset = imageOffset;
        mImageCount = imageCount;
        mNumTopics = (list == null) ? 0 : list.size();
        this.rowHeight = rowHeight;
    }

    public int getCount() {
        int count = mImageCount;
        if (mImageOffset + mImageCount >= mNumTopics)
            count = mNumTopics - mImageOffset;
        return count;
    }

    @Override
    public GridImageAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.card_view_menu, null);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GridImageAdapter.ViewHolder viewHolder, int position) {
        DataMenu fp = mArrayList.get(position);

        viewHolder.tvVersionName.setText(fp.getName());
        Glide.with(mContext).load(fp.getPicture()).into(viewHolder.iconView);

        int numRows = (getCount() - 1) / mContext.getResources().getInteger(R.integer.num_cols);
        int dividerShow = mContext.getResources().getInteger(R.integer.num_cols);

        if (numRows > 1)
            dividerShow = numRows * mContext.getResources().getInteger(R.integer.num_cols);
        else if (numRows == 0 && getCount() <= mContext.getResources().getInteger(R.integer.num_cols))
            dividerShow = 0;
        else if (numRows <= 1 && getCount() > mContext.getResources().getInteger(R.integer.num_cols))
            dividerShow = 1 * mContext.getResources().getInteger(R.integer.num_cols);
        ;

        viewHolder.feed = fp;
    }

    public long getItemId(int position) {
        return mImageOffset + position;
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvVersionName;
        public ImageView iconView;

        public DataMenu feed;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            tvVersionName = (TextView) itemLayoutView
                    .findViewById(R.id.tvVersionName);
            iconView = (ImageView) itemLayoutView
                    .findViewById(R.id.iconId);

            itemLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                 /*   Intent intent = new Intent(v.getContext(), SecondPage.class);
                    v.getContext().startActivity(intent);
                    Toast.makeText(v.getContext(), "os version is: " + feed.getTitle(), Toast.LENGTH_SHORT).show();*/
                }
            });
        }

    }
}

