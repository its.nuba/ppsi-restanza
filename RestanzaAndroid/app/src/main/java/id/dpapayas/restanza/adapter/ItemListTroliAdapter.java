package id.dpapayas.restanza.adapter;

/**
 * Created by dpapayas on 11/6/16.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.dpapayas.restanza.R;
import id.dpapayas.restanza.realm.RealmController;
import id.dpapayas.restanza.realm.object.DataMenuRealm;
import id.dpapayas.restanza.util.TotalValTroliEvent;

public class ItemListTroliAdapter extends RecyclerView.Adapter<ItemListTroliAdapter.MyViewHolder> {

    private Context mContext;
    private List<DataMenuRealm> produkDataList;
    List<String> listSpinner = new ArrayList<>();
    Fragment fragment;
    long hasil;
    EventBus eventBus;


    HashMap<Integer, Long> intString = new HashMap<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPrice, tvName;
        public ImageView thumbnail, btnDelete;
        public Spinner spKuantiti;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvPrice = (TextView) view.findViewById(R.id.tvPrice);
            spKuantiti = (Spinner) view.findViewById(R.id.spKuantiti);

            btnDelete = (ImageView) view.findViewById(R.id.btnDelete);
            thumbnail = (ImageView) view.findViewById(R.id.imgMenu);

            listSpinner.add("1");
            listSpinner.add("2");
            listSpinner.add("3");
            listSpinner.add("4");
            listSpinner.add("5");
            listSpinner.add("6");
            listSpinner.add("7");
            listSpinner.add("8");
            listSpinner.add("9");
            listSpinner.add("10");


            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, listSpinner);
            spKuantiti.setAdapter(adapter);

        }
    }


    public ItemListTroliAdapter(Context mContext, List<DataMenuRealm> produkDataList, EventBus eventBus) {
        this.mContext = mContext;
        this.produkDataList = produkDataList;
        this.eventBus = eventBus;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_order, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DataMenuRealm produkData = produkDataList.get(position);

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);


        holder.tvName.setText(produkData.getName());

        final long valLHasilDp = produkData.getPrice();

        holder.tvPrice.setText(kursIndonesia.format(produkData.getPrice()) + "");

        Glide.with(mContext).load(produkData.getPicture()).into(holder.thumbnail);

        holder.spKuantiti.setTag(position);

        holder.spKuantiti.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String getSpinner = listSpinner.get(pos);
                int spinVal = Integer.parseInt(getSpinner);

                hasil = valLHasilDp * spinVal;

                intString.put(position, hasil);

                eventBus.post(new TotalValTroliEvent(intString));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(mContext)
                        .content("Anda yakin akan menghapus?")
                        .positiveText("Ya")
                        .negativeText("Tidak")
                        .neutralText("Cancel")
                        .positiveColor(Color.parseColor("#CA675B"))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                RealmController.getInstance().DeleteOrder(position);
                            }
                        })
                        .show();
            }
        });
    }

    public void onItemClick(AdapterView<?> a, View v, int position, long id) {
        AlertDialog.Builder adb = new AlertDialog.Builder(mContext);
        adb.setTitle("Delete?");
        adb.setMessage("Are you sure you want to delete " + position);
        final int positionToRemove = position;
        adb.setNegativeButton("Cancel", null);
        adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        adb.show();
    }

//    public double getTotal(ArrayList<TroliDataRealm> list){
//
//    }

    @Override
    public int getItemCount() {
        return produkDataList.size();
    }

}
