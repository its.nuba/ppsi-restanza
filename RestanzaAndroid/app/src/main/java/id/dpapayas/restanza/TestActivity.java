package id.dpapayas.restanza;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import id.dpapayas.restanza.api.APIClient;
import id.dpapayas.restanza.api.APIInterface;
import id.dpapayas.restanza.api.model.DataMenu;
import id.dpapayas.restanza.api.model.MenuResponse;
import id.dpapayas.restanza.fragment.FragmentPageAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dpapayas on 5/19/17.
 */

public class TestActivity extends AppCompatActivity {

    LinearLayout llDots;
    List<DataMenu> dataMenuList;
    FragmentPageAdapter mAdapter;
    ViewPager mPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dashboard);

        GetFood(0);


    }



    private void GetFood(int type) {
        APIInterface apiService =
                APIClient.getClient().create(APIInterface.class);

        Call<MenuResponse> call = apiService.getAllFood(type);
        call.enqueue(new Callback<MenuResponse>() {
            @Override
            public void onResponse(Call<MenuResponse> call, Response<MenuResponse> response) {
                dataMenuList = response.body().getData();
                mAdapter = new FragmentPageAdapter(getSupportFragmentManager(), dataMenuList, getResources());

            }

            @Override
            public void onFailure(Call<MenuResponse> call, Throwable t) {
                Log.d("ERROR", t.toString());
            }

        });
    }


}