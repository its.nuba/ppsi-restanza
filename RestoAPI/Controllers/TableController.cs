﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RestoAPI.Controllers
{
    using Helpers;
    using Models;
    using Models.DB;
    using Repositories;

    [Route("api/[controller]")]
    public class TableController : Controller
    {
        private UserRepository _userRepo;
        private readonly OrderRepository _orderRepo;

        public TableController(UserRepository userRepo, OrderRepository orderRepo)
        {
            _userRepo = userRepo;
            _orderRepo = orderRepo;
        }

        [HttpGet]
        public ResponseAPI Get()
        {
            var table = _orderRepo.GetTableList();

            return new ResponseAPI(table.Count() > 0, ResponseAPI.ResponseType.Read, table);
        }
        
        [HttpGet("{id}/Order")]
        public ResponseAPI GetTableOrder(int id)
        {
            var order = _orderRepo.GetTableOrderList(id);

            return new ResponseAPI(order != null, ResponseAPI.ResponseType.Read, order);
        }

        [HttpGet("{id}/Order/all")]
        public ResponseAPI GetTableOrderAll(int id)
        {
            var order = _orderRepo.GetTableOrderList(id, true);

            return new ResponseAPI(order != null, ResponseAPI.ResponseType.Read, order);
        }

        [HttpGet("{id}/LastOrder")]
        public ResponseAPI GetLastTableOrder(int id)
        {
            var order = _orderRepo.GetTableOrderLast(id);

            return new ResponseAPI(order != null, ResponseAPI.ResponseType.Read, order);
        }

        [HttpGet("{id}/LastOrder/all")]
        public ResponseAPI GetLastTableOrderAll(int id)
        {
            var order = _orderRepo.GetTableOrderLast(id, true);

            return new ResponseAPI(order != null, ResponseAPI.ResponseType.Read, order);
        }

        [HttpPost]
        public ResponseAPI Post([FromBody]Table data)
        {
            var table = new Table
            {
                Name = data.Name,
                Capacity = data.Capacity
            };

            try
            {
                var id = _userRepo.AddTable(table);

                var createdTable = _userRepo.GetTable(id);

                return new ResponseAPI(true, ResponseAPI.ResponseType.Create, createdTable);
            }
            catch { }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Create, null);
        }

        [HttpPut]
        public ResponseAPI Put([FromBody]Table data)
        {
            var exist = _userRepo.GetTable(data.Id);
            if (exist != null)
            {
                exist.Name = data.Name;
                exist.Capacity = data.Capacity;
                
                try
                {
                    var id = _userRepo.EditTable(exist);

                    var editedTable = _userRepo.GetTable(id);

                    return new ResponseAPI(true, ResponseAPI.ResponseType.Update, editedTable);
                }
                catch { }

                return new ResponseAPI(false, ResponseAPI.ResponseType.Update, null);
            }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Read, null);
        }

        [HttpDelete("{id}")]
        public ResponseAPI Delete(int id)
        {
            var exist = _userRepo.GetTable(id);
            if (exist != null)
            {
                try
                {
                    var status = _userRepo.DeleteTable(exist.Id);

                    return new ResponseAPI(status, ResponseAPI.ResponseType.Delete);
                }
                catch { }

                return new ResponseAPI(false, ResponseAPI.ResponseType.Delete);
            }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Read, null);
        }
    }
}
