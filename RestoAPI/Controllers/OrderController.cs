﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RestoAPI.Controllers
{
    using Helpers;
    using Models;
    using Models.DB;
    using Repositories;

    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private MenuRepository _menuRepo;
        private OrderRepository _orderRepo;

        public OrderController(OrderRepository orderRepo, MenuRepository menuRepo)
        {
            _orderRepo = orderRepo;
            _menuRepo = menuRepo;
        }

        [HttpGet("Status")]
        public ResponseAPI GetOrderStatus()
        {
            var status = _orderRepo.GetOrderStatusList();

            return new ResponseAPI(status.Count() > 0, ResponseAPI.ResponseType.Read, status);
        }

        [HttpGet("DetailStatus")]
        public ResponseAPI GetOrderDetailStatus()
        {
            var status = _orderRepo.GetOrderDetailStatusList();

            return new ResponseAPI(status.Count() > 0, ResponseAPI.ResponseType.Read, status);
        }

        [HttpPost]
        public ResponseAPI Post([FromBody]Order data)
        {
            var table = _orderRepo.GetTableLastStatus(data.TableId);

            if (table != null && table.Status == Order.OrderStatus.Available)
            {
                var order = new Order
                {
                    CheckInOn = DateTime.Now,
                    TableId = data.TableId,
                    Status = Order.OrderStatus.Ordering,
                    TotalAmount = 0,
                    TotalTax = 0
                };

                try
                {
                    var id = _orderRepo.AddOrder(order);

                    var createdOrder = _orderRepo.GetOrder(id);

                    return new ResponseAPI(true, ResponseAPI.ResponseType.Create, createdOrder);
                }
                catch { }
            }
            
            return new ResponseAPI(false, ResponseAPI.ResponseType.Create, null);
        }
        
        [HttpPut]
        public ResponseAPI Put([FromBody]Order data)
        {
            var exist = _orderRepo.GetOrder(data.Id);
            if (exist != null)
            {

                if (exist.Status != Order.OrderStatus.Finished)
                {
                    exist.Status = data.Status;
                    if (data.Status == Order.OrderStatus.Finished) exist.CheckOutOn = DateTime.Now;

                    try
                    {
                        var id = _orderRepo.EditOrder(exist);

                        var editedOrder = _orderRepo.GetOrder(id);

                        return new ResponseAPI(true, ResponseAPI.ResponseType.Update, editedOrder);
                    }
                    catch { }
                }

                return new ResponseAPI(false, ResponseAPI.ResponseType.Update, null);
            }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Read, null);
        }
        
        [HttpPost("{id}/Detail")]
        public ResponseAPI PostDetail([FromBody]List<OrderDetail> data, int id)
        {
            var order = _orderRepo.GetOrder(id);
            if (order != null && order.Status != Order.OrderStatus.Finished)
            {
                try
                {
                    //check if id match
                    if (data.Any(x => x.OrderId != id)) throw new Exception();

                    foreach (var item in data)
                    {
                        var orderDetail = new OrderDetail
                        {
                            OrderId = id,
                            CreatedBy = item.CreatedBy,
                            CreatedOn = DateTime.Now,
                            MenuId = item.MenuId,
                            Note = item.Note,
                            TotalQuantity = item.TotalQuantity
                        };

                        var idd = _orderRepo.AddOrderDetail(orderDetail);

                        if (idd > 0)
                        {
                            var menu = _menuRepo.GetMenu(orderDetail.MenuId);
                            order.TotalAmount += menu.Price * orderDetail.TotalQuantity;
                            order.TotalTax = order.TotalAmount / 10;
                            order.Status = Order.OrderStatus.Waiting;
                            _orderRepo.EditOrder(order);

                            _orderRepo.AddOrderDetailStatus(new OrderDetailStatus
                            {
                                CreatedBy = orderDetail.CreatedBy,
                                CreatedOn = DateTime.Now,
                                OrderDetailId = idd,
                                Status = OrderDetailStatus.StatusType.Queue
                            });
                        }
                    }

                    var createdOrderDetail = _orderRepo.GetOrderDetailListWithStatus(id);

                    return new ResponseAPI(true, ResponseAPI.ResponseType.Create, createdOrderDetail);
                }
                catch { }
            }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Create, null);
        }

        [HttpPut("{id}/Detail")]
        public ResponseAPI PutDetail([FromBody]OrderDetail data, int id)
        {
            var order = _orderRepo.GetOrder(id);
            if (order != null && order.Status != Order.OrderStatus.Finished)
            {
                var exist = _orderRepo.GetOrderDetail(data.Id);
                if (exist != null && exist.OrderId == id)
                {
                    var lastStatus = _orderRepo.GetLastOrderDetailStatus(data.Id);
                    if (lastStatus == null || lastStatus.Status == OrderDetailStatus.StatusType.Queue)
                    {
                        exist.UpdateBy = data.UpdateBy;
                        exist.UpdateOn = DateTime.Now;
                        exist.Note = data.Note;

                        try
                        {
                            if (exist.TotalQuantity != data.TotalQuantity)
                            {
                                var menu = _menuRepo.GetMenu(exist.MenuId);
                                order.TotalAmount -= menu.Price * exist.TotalQuantity;
                                order.TotalAmount += menu.Price * data.TotalQuantity;
                                order.TotalTax = order.TotalAmount / 10;

                                _orderRepo.EditOrder(order);

                                exist.TotalQuantity = data.TotalQuantity;
                            }

                            var idd = _orderRepo.EditOrderDetail(exist);

                            var editedOrder = _orderRepo.GetOrderDetailWithStatus(idd);

                            return new ResponseAPI(true, ResponseAPI.ResponseType.Update, editedOrder);
                        }
                        catch { }

                        return new ResponseAPI(false, ResponseAPI.ResponseType.Update, null);
                    }
                }
            }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Read, null);
        }


        [HttpPost("{id}/Detail/Status")]
        public ResponseAPI PostDetailStatus([FromBody]OrderDetailStatus data, int id)
        {
            var order = _orderRepo.GetOrder(id);
            if (order != null && order.Status != Order.OrderStatus.Finished)
            {
                var orderDetailStatus = new OrderDetailStatus
                {
                    CreatedBy = data.CreatedBy,
                    CreatedOn = DateTime.Now,
                    OrderDetailId = data.OrderDetailId,
                    Status = data.Status
                };
                var orderDetail = _orderRepo.GetOrderDetail(orderDetailStatus.OrderDetailId);
                if (orderDetail != null && orderDetail.OrderId == id)
                {
                    var lastStatus = _orderRepo.GetLastOrderDetailStatus(orderDetail.Id);
                    if (lastStatus == null || lastStatus.Status != OrderDetailStatus.StatusType.Canceled)
                    {
                        try
                        {
                            var ids = _orderRepo.AddOrderDetailStatus(orderDetailStatus);

                            if (ids > 0 && orderDetailStatus.Status == OrderDetailStatus.StatusType.Canceled)
                            {
                                if (orderDetailStatus.Status == OrderDetailStatus.StatusType.Canceled)
                                {
                                    var menu = _menuRepo.GetMenu(orderDetail.MenuId);
                                    order.TotalAmount -= menu.Price * orderDetail.TotalQuantity;
                                    order.TotalTax = order.TotalAmount / 10;
                                }
                                else if (orderDetailStatus.Status == OrderDetailStatus.StatusType.Delivered)
                                {
                                    var OrderDetailListWithStatus = _orderRepo.GetOrderDetailListWithStatus(order.Id);
                                    if (!OrderDetailListWithStatus.Any(x => x.LastStatus == OrderDetailStatus.StatusType.Delivered))
                                    {
                                        order.Status = Order.OrderStatus.Completed;
                                    }
                                }

                                _orderRepo.EditOrder(order);
                            }

                            var lastOrderStatus = _orderRepo.GetOrderDetailWithStatus(orderDetail.Id);

                            return new ResponseAPI(true, ResponseAPI.ResponseType.Create, lastOrderStatus);
                        }
                        catch { }
                    }
                }
            }
            
            return new ResponseAPI(false, ResponseAPI.ResponseType.Create, null);
        }

        [HttpGet("Dummy/EmptyAll")]
        public ResponseAPI EmptyTransaction()
        {
            try
            {
                _orderRepo.DeleteTransactionalData();

                return new ResponseAPI(true, ResponseAPI.ResponseType.Delete, null);
            }
            catch { }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Delete, null);
        }

        [HttpGet("Dummy/Add")]
        public ResponseAPI AddTransaction()
        {
            try
            {
                _orderRepo.InsertTransactionalData();

                return new ResponseAPI(true, ResponseAPI.ResponseType.Create, null);
            }
            catch { }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Create, null);
        }

        [HttpGet("AllOrderDetail")]
        public ResponseAPI GetAllOrderDetail()
        {
            var data = _orderRepo.GetAllOrderDetailForToday();

            return new ResponseAPI(data.Count() > 0, ResponseAPI.ResponseType.Read, data);
        }
    }
}
