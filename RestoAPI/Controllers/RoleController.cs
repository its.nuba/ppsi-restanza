﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RestoAPI.Controllers
{
    using Helpers;
    using Models;
    using Models.DB;
    using Repositories;

    [Route("api/[controller]")]
    public class RoleController : Controller
    {
        private UserRepository _userRepo;

        public RoleController(UserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        [HttpGet]
        public ResponseAPI Get()
        {
            var role = _userRepo.GetRoleList();

            return new ResponseAPI(role.Count() > 0, ResponseAPI.ResponseType.Read, role);
        }
        
        [HttpGet("{id}")]
        public ResponseAPI Get(int id)
        {
            var role = _userRepo.GetRole(id);

            return new ResponseAPI(role != null, ResponseAPI.ResponseType.Read, role);
        }
    }
}
