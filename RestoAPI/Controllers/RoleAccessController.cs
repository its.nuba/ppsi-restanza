﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RestoAPI.Controllers
{
    using Helpers;
    using Models;
    using Models.DB;
    using Repositories;

    [Route("api/[controller]")]
    public class RoleAccessController : Controller
    {
        private UserRepository _userRepo;

        public RoleAccessController(UserRepository userRepo)
        {
            _userRepo = userRepo;
        }
        
        [HttpGet("{id}")]
        public ResponseAPI Get(int id)
        {
            var roleAccess = _userRepo.GetRoleAccessList(id);

            return new ResponseAPI(roleAccess != null, ResponseAPI.ResponseType.Read, roleAccess);
        }
    }
}
