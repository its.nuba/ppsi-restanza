﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RestoAPI.Controllers
{
    using Helpers;
    using Models;
    using Models.DB;
    using Repositories;

    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private UserRepository _userRepo;

        public LoginController(UserRepository userRepo)
        {
            _userRepo = userRepo;
        }
        
        [HttpPost]
        public ResponseAPI Post([FromBody]LoginRequest data)
        {
            var success = false;
            User result = null;

            try
            {
                success = _userRepo.CheckUsernamePassword(data.Username, data.Password);
                result = _userRepo.GetUser(data.Username);
            }
            catch { }

            return new ResponseAPI(success, "Login successfully", "Wrong username or password", result);
        }
        
        public class LoginRequest
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }
    }
}
