﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RestoAPI.Controllers
{
    using Helpers;
    using Models;
    using Models.DB;
    using Repositories;

    [Route("api/[controller]")]
    public class MenuController : Controller
    {
        private readonly MenuRepository _menuRepo;

        public MenuController(MenuRepository menuRepo)
        {
            _menuRepo = menuRepo;
        }

        [HttpGet]
        public ResponseAPI Get()
        {
            var menu = _menuRepo.GetMenuList();

            return new ResponseAPI(menu.Count() > 0, ResponseAPI.ResponseType.Read, menu);
        }
        
        [HttpGet("{id}")]
        public ResponseAPI Get(int id)
        {
            var menu = _menuRepo.GetMenu(id, true);

            return new ResponseAPI(menu != null, ResponseAPI.ResponseType.Read, menu);
        }

        [HttpGet("FoodType")]
        public ResponseAPI GetFoodType()
        {
            var foodType = _menuRepo.GetFoodTypeList();

            return new ResponseAPI(foodType.Count() > 0, ResponseAPI.ResponseType.Read, foodType);
        }

        [HttpGet("{id}/WithFoodType")]
        public ResponseAPI GetMenuWithFoodType(int id)
        {
            var foodType = _menuRepo.GetMenuWithType(id);

            return new ResponseAPI(foodType!= null, ResponseAPI.ResponseType.Read, foodType);
        }

        [HttpGet("WithFoodType")]
        public ResponseAPI GetMenuWithFoodType()
        {
            var foodType = _menuRepo.GetMenuWithTypeList();

            return new ResponseAPI(foodType.Count() > 0, ResponseAPI.ResponseType.Read, foodType);
        }

        [HttpGet("WithFoodType/{id}")]
        public ResponseAPI GetMenuWithFoodType(string id)
        {
            var foodType = _menuRepo.GetMenuWithTypeList(id);

            return new ResponseAPI(foodType.Count() > 0, ResponseAPI.ResponseType.Read, foodType);
        }

        [HttpPost]
        public ResponseAPI Post([FromBody]Menu data)
        {
            var menu = new Menu
            {
                CreatedBy = data.CreatedBy,
                CreatedOn = data.CreatedOn,
                Description = data.Description,
                FoodType = data.FoodType,
                IsActive = data.IsActive,
                IsRecomended = data.IsRecomended,
                Name = data.Name,
                Picture = data.Picture,
                Price = data.Price
            };

            try
            {
                var id = _menuRepo.AddMenu(menu);

                var createdMenu = _menuRepo.GetMenu(id, true);

                return new ResponseAPI(true, ResponseAPI.ResponseType.Create, createdMenu);
            }
            catch { }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Create, null);
        }

        [HttpPut]
        public ResponseAPI Put([FromBody]Menu data)
        {
            var exist = _menuRepo.GetMenu(data.Id);
            if (exist != null)
            {
                exist.Name = data.Name;
                exist.Description = data.Description;
                exist.FoodType = data.FoodType;
                exist.IsActive = data.IsActive;
                exist.IsRecomended = data.IsRecomended;
                exist.Name = data.Name;
                if(data.Picture != null) exist.Picture = data.Picture;
                exist.Price = data.Price;
                exist.UpdateBy = data.UpdateBy;
                exist.UpdateOn = data.UpdateOn;

                try
                {
                    var id = _menuRepo.EditMenu(exist);

                    var editedMenu = _menuRepo.GetMenu(id, true);

                    return new ResponseAPI(true, ResponseAPI.ResponseType.Update, editedMenu);
                }
                catch { }

                return new ResponseAPI(false, ResponseAPI.ResponseType.Update, null);
            }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Read, null);
        }

        [HttpDelete("{id}")]
        public ResponseAPI Delete(int id)
        {
            var exist = _menuRepo.GetMenu(id);
            if (exist != null)
            {
                try
                {
                    var status = _menuRepo.DeleteMenu(exist.Id);

                    return new ResponseAPI(status, ResponseAPI.ResponseType.Delete);
                }
                catch { }

                return new ResponseAPI(false, ResponseAPI.ResponseType.Delete);
            }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Read, null);
        }
    }
}
