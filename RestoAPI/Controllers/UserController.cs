﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace RestoAPI.Controllers
{
    using Helpers;
    using Models;
    using Models.DB;
    using Repositories;

    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private UserRepository _userRepo;

        public UserController(UserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        [HttpGet]
        public ResponseAPI Get()
        {
            var user = _userRepo.GetUserList();

            return new ResponseAPI(user.Count() > 0, ResponseAPI.ResponseType.Read, user);
        }

        [HttpGet("WithRole")]
        public ResponseAPI GetUserWithRole()
        {
            var user = _userRepo.GetUserOnlyList();

            return new ResponseAPI(user.Count() > 0, ResponseAPI.ResponseType.Read, user);
        }

        [HttpGet("WithTable")]
        public ResponseAPI GetUserWithTable()
        {
            var user = _userRepo.GetUserTableList();

            return new ResponseAPI(user.Count() > 0, ResponseAPI.ResponseType.Read, user);
        }

        [HttpGet("{id}")]
        public ResponseAPI Get(string id)
        {
            var user = _userRepo.GetUser(id);

            return new ResponseAPI(user != null, ResponseAPI.ResponseType.Read, user);
        }

        [HttpGet("{id}/WithTable")]
        public ResponseAPI GetUserWithTable(int id)
        {
            var user = _userRepo.GetUserTable(id);

            return new ResponseAPI(user != null, ResponseAPI.ResponseType.Read, user);
        }

        [HttpPost]
        public ResponseAPI Post([FromBody]User data)
        {
            var user = new User {
                CreatedBy = data.CreatedBy,
                CreatedOn = data.CreatedOn,
                IsActive = data.IsActive,
                IsTable = data.IsTable,
                Name = data.Name,
                Password = Encryption.GetHash(data.Password),
                RoleId = data.RoleId,
                TableId = data.TableId,
                Username = data.Username
            };
            
            try
            {
                var id = _userRepo.AddUser(user);

                var createdUser = _userRepo.GetUser(id);

                return new ResponseAPI(true, ResponseAPI.ResponseType.Create, createdUser);
            }
            catch { }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Create, null);
        }
        
        [HttpPut]
        public ResponseAPI Put([FromBody]User data)
        {
            var exist = _userRepo.GetUser(data.Id);
            if (exist != null)
            {
                exist.IsActive = data.IsActive;
                exist.IsTable = data.IsTable;
                exist.Name = data.Name;
                exist.RoleId = data.RoleId;
                //exist.TableId = data.TableId;
                exist.UpdateBy = data.UpdateBy;
                exist.UpdateOn = data.UpdateOn;
                exist.Username = data.Username;

                //passwword change
                if (!string.IsNullOrEmpty(data.Password))
                {
                    exist.Password = Encryption.GetHash(data.Password);
                }
                
                try
                {
                    var id = _userRepo.EditUser(exist);

                    var editedUser = _userRepo.GetUser(id);

                    return new ResponseAPI(true, ResponseAPI.ResponseType.Update, editedUser);
                }
                catch { }

                return new ResponseAPI(false, ResponseAPI.ResponseType.Update, null);
            }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Read, null);
        }
        
        [HttpDelete("{id}")]
        public ResponseAPI Delete(string id)
        {
            var exist = _userRepo.GetUser(id);
            if (exist != null)
            {
                try
                {
                    var status = _userRepo.DeleteUser(exist.Id);

                    return new ResponseAPI(status, ResponseAPI.ResponseType.Delete);
                }
                catch { }

                return new ResponseAPI(false, ResponseAPI.ResponseType.Delete);
            }

            return new ResponseAPI(false, ResponseAPI.ResponseType.Read, null);
        }
    }
}
