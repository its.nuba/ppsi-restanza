﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Collections.Generic;
using System.Linq;
using System;

namespace RestoAPI.Models
{
    using DB;
    using Helpers;

    public class RestoContextInitializer : DbContext
    {
        public static void Initializer(RestoContext context)
        {
            context.Database.EnsureCreated();

            if (context.Access.Any())
            {
                return;
            }

            foreach (Access.ModuleType item in Enum.GetValues(typeof(Access.ModuleType)))
            {
                foreach (Access.AccessType subitem in Enum.GetValues(typeof(Access.AccessType)))
                {
                    var moduleName = Enum.GetName(typeof(Access.ModuleType), item);
                    var typeName = Enum.GetName(typeof(Access.AccessType), subitem);
                    var newAccess = new Access { Module = item, Type = subitem, Name = moduleName + " - " + typeName };

                    context.Add(newAccess);
                }
            }
            context.SaveChanges();

            context.Add(new Role { Name = "Admin", IsActive = true, CreatedBy = 0, CreatedOn = DateTime.Now });
            context.Add(new Role { Name = "Cashier", IsActive = true, CreatedBy = 0, CreatedOn = DateTime.Now });
            context.Add(new Role { Name = "Waiter", IsActive = true, CreatedBy = 0, CreatedOn = DateTime.Now });
            context.Add(new Role { Name = "Cook", IsActive = true, CreatedBy = 0, CreatedOn = DateTime.Now });
            context.Add(new Role { Name = "Table", IsActive = true, CreatedBy = 0, CreatedOn = DateTime.Now });
            context.SaveChanges();

            context.Add(new User { Name = "Admin", Username = "admin", Password = Encryption.GetHash("pass"), RoleId = 1, IsActive = true, CreatedBy = 0, CreatedOn = DateTime.Now });
            context.SaveChanges();

            context.Add(new User { Name = "Kasir", Username = "kasir", Password = Encryption.GetHash("pass"), RoleId = 2, IsActive = true, CreatedBy = 0, CreatedOn = DateTime.Now });
            context.SaveChanges();

            context.Add(new User { Name = "Pelayan", Username = "pelayan", Password = Encryption.GetHash("pass"), RoleId = 3, IsActive = true, CreatedBy = 0, CreatedOn = DateTime.Now });
            context.SaveChanges();

            context.Add(new User { Name = "Koki", Username = "koki", Password = Encryption.GetHash("pass"), RoleId = 4, IsActive = true, CreatedBy = 0, CreatedOn = DateTime.Now });
            context.SaveChanges();

            foreach (var item in context.Access.Select(x => x.Id).ToList())
            {
                var newRoleAccess = new RoleAccess { RoleId = 1, AccessId = item };
                context.Add(newRoleAccess);
            }
            context.SaveChanges();

            context.Add(new Table { Name = "Table A1", Capacity = 2 });
            context.Add(new Table { Name = "Table A2", Capacity = 2 });
            context.Add(new Table { Name = "Table A3", Capacity = 2 });
            context.Add(new Table { Name = "Table B1", Capacity = 4 });
            context.SaveChanges();

            foreach (var item in context.Table.ToList())
            {
                context.Add(new User { Name = item.Name, Username = "table-" + item.Name.Split(' ').LastOrDefault().ToLower(), Password = Encryption.GetHash("pass"), RoleId = 5, IsActive = true, IsTable = true, CreatedBy = 0, CreatedOn = DateTime.Now, TableId = item.Id });
            }
            context.SaveChanges();

            for (var i = 1; i < 4; i++)
            {
                context.Add(new Menu { Name = "Food " + i.ToString(), CreatedBy = 0, CreatedOn = DateTime.Now, Description = "Food Description", FoodType = Menu.FoodTypes.Food, IsActive = true, IsRecomended = i > 2, Picture = "", Price = 10000 + 5000 * i });
            }
            context.SaveChanges();

            for (var i = 1; i < 2; i++)
            {
                context.Add(new Menu { Name = "Snack " + i.ToString(), CreatedBy = 0, CreatedOn = DateTime.Now, Description = "Snack Description", FoodType = Menu.FoodTypes.Snack, IsActive = true, IsRecomended = false, Picture = "", Price = 5000 * i });
            }
            context.SaveChanges();

            for (var i = 1; i < 3; i++)
            {
                context.Add(new Menu { Name = "Beverage " + i.ToString(), CreatedBy = 0, CreatedOn = DateTime.Now, Description = "Beverage Description", FoodType = Menu.FoodTypes.Beverage, IsActive = true, IsRecomended = true, Picture = "", Price = 10000 + 2000 * i });
            }
            context.SaveChanges();
        }
    }
}
