﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoAPI.Models
{
    public class ResponseAPI
    {
        public enum ResponseType
        {
            Create,
            Read,
            Update,
            Delete
        }

        public ResponseAPI(bool success, ResponseType type, dynamic data = null)
        {
            Status = success;

            var msg = "";
            switch (type)
            {
                case ResponseType.Create:
                    msg = Status ? "Data successfully addedd" : "Can not add data";
                    break;
                case ResponseType.Read:
                    msg = Status ? "Data found" : "Data not found";
                    break;
                case ResponseType.Update:
                    msg = Status ? "Data successfully modified" : "Can not modify data";
                    break;
                case ResponseType.Delete:
                    msg = Status ? "Data successfully removed" : "Can not remove data";
                    break;
            }
            Message = msg;

            Data = data;
        }
        
        public ResponseAPI(bool success, string successMsg, string failedMsg, dynamic data = null)
        {
            Status = success;
            Message = success ? successMsg : failedMsg;
            Data = data;
        }

        public bool Status { get; set; }

        public string Message { get; set; }

        public dynamic Data { get; set; }
    }
}
