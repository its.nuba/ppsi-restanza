﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoAPI.Models.Ext
{
    using DB;

    public class MenuWithType : Menu
    {
        public new string FoodType { get; set; }
    }
}
