﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoAPI.Models.Ext
{
    using DB;

    public class OrderWithStatus : Order
    {
        public string StatusText { get; set; }

        public double TotalTax { get; set; }

        public new IEnumerable<OrderDetailWithStatus> OrderDetail { get; set; }
    }
}
