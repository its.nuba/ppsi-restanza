﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoAPI.Models.Ext
{
    using DB;
    public class UserWithRole
    {
        public User user { get; set; }

        public Role role { get; set; }
    }
}
