﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoAPI.Models.Ext
{
    using DB;

    public class OrderDetailWithStatus : OrderDetail
    {
        public OrderDetailStatus.StatusType LastStatus { get; set; }

        public string LastStatusText { get; set; }

        public DateTime LastStatusTime { get; set; }

        public new string Menu { get; set; }

        public double MenuPrice { get; set; }

        public int TableId { get; set; }

        public string TableName { get; set; }
    }
}
