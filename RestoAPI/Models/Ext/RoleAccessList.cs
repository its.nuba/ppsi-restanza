﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoAPI.Models.Ext
{
    using DB;

    public class RoleAccessList
    {
        public int idRole { get; set; }

        public Access.AccessType type { get; set; }

        public Access.ModuleType module { get; set; }
    }
}
