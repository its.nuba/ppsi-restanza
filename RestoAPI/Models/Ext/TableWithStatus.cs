﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoAPI.Models.Ext
{
    using DB;

    public class TableWithStatus : Table
    {
        public Order.OrderStatus Status { get; set; }

        public string StatusText { get; set; }

        public int FinishedOrder { get; set; }

        public int TotalOrder { get; set; }

        public double TotalAmount { get; set; }

        public double TotalTax { get; set; }

        public double GrandTotal { get; set; }
    }
}
