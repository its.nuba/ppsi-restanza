﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoAPI.Models.Ext
{
    using DB;
    public class UserWithTable
    {
        public User user { get; set; }

        public Table table { get; set; }
    }
}
