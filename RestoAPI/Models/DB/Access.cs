﻿namespace RestoAPI.Models.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Access : XI_Identity
    {
        public Access()
        {
            RoleAccess = new HashSet<RoleAccess>();
        }

        public enum AccessType
        {
            Create,
            Read,
            Update,
            Delete
        }

        public enum ModuleType
        {
            MasterUser,
            MasterRole,
            MasterTable,
            MasterMenu,
        }

        [Required, MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public AccessType Type { get; set; }

        [Required]
        public ModuleType Module { get; set; }

        public virtual ICollection<RoleAccess> RoleAccess { get; set; }
    }
}
