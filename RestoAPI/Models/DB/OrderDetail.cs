﻿namespace RestoAPI.Models.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class OrderDetail : XI_Modify
    {
        public OrderDetail()
        {
            OrderDetailStatus = new HashSet<OrderDetailStatus>();
        }

        [Required]
        public int OrderId { get; set; }

        [Required]
        public int MenuId { get; set; }
        
        [Required]
        public int TotalQuantity { get; set; }
        
        public string Note { get; set; }
        
        [ForeignKey(nameof(OrderId))]
        public virtual Order Order { get; set; }

        [ForeignKey(nameof(MenuId))]
        public virtual Menu Menu { get; set; }

        public virtual ICollection<OrderDetailStatus> OrderDetailStatus { get; set; }
    }
}
