﻿namespace RestoAPI.Models.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Menu : XI_Modify
    {
        public enum FoodTypes
        {
            Food,
            Snack,
            Beverage
        }

        [Required]
        public FoodTypes FoodType { get; set; }

        [Required, MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public double Price { get; set; }

        [Required]
        public string Picture { get; set; }

        public bool IsRecomended { get; set; }
        
        public bool IsActive { get; set; }
    }
}
