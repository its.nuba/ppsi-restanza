﻿namespace RestoAPI.Models.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class AppConfig : XI_Identity
    {
        [Required, MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public string Value { get; set; }
        
        public string Description { get; set; }
    }
}
