﻿namespace RestoAPI.Models.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class RoleAccess : XI_Identity
    {
        [Required]
        public int RoleId { get; set; }

        [Required]
        public int AccessId { get; set; }
        
        [ForeignKey(nameof(RoleId))]
        public virtual Role Role { get; set; }
        
        [ForeignKey(nameof(AccessId))]
        public virtual Access Access { get; set; }
    }
}
