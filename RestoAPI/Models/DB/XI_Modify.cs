﻿namespace RestoAPI.Models.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class XI_Modify : XI_Created
    {
        public DateTime? UpdateOn { get; set; }

        public int? UpdateBy { get; set; }
    }
}
