﻿namespace RestoAPI.Models.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Order : XI_Identity
    {
        public Order()
        {
            CheckInOn = DateTime.Now;

            OrderDetail = new HashSet<OrderDetail>();
        }

        public enum OrderStatus
        {
            Available,
            Ordering,
            Waiting,
            Completed,
            Finished
        }

        [Required]
        public int TableId { get; set; }
        
        [Required]
        public double TotalAmount { get; set; }

        [Required]
        public double TotalTax { get; set; }

        [Required]
        public DateTime CheckInOn { get; set; }

        public DateTime? CheckOutOn { get; set; }

        public OrderStatus Status { get; set; }

        [ForeignKey(nameof(TableId))]
        public virtual Table Table { get; set; }

        public virtual ICollection<OrderDetail> OrderDetail { get; set; }
    }
}
