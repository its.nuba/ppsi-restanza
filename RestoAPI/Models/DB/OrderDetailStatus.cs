﻿namespace RestoAPI.Models.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class OrderDetailStatus : XI_Created
    {
        public enum StatusType
        {
            Queue,
            Progress,
            Delivered,
            Canceled
        }

        [Required]
        public int OrderDetailId { get; set; }

        [Required]
        public StatusType Status { get; set; }

        [ForeignKey(nameof(OrderDetailId))]
        public virtual OrderDetail OrderDetail { get; set; }
    }
}
