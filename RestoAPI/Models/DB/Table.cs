﻿namespace RestoAPI.Models.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Table : XI_Identity
    {
        [Required, MaxLength(50)]
        public string Name { get; set; }

        public int Capacity { get; set; }
    }
}
