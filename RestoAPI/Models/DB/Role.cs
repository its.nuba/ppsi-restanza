﻿namespace RestoAPI.Models.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Role : XI_Modify
    {
        public Role()
        {
            RoleAccess = new HashSet<RoleAccess>();
        }

        [Required, MaxLength(50)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<RoleAccess> RoleAccess { get; set; }
    }
}
