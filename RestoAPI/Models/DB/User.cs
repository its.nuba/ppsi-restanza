﻿namespace RestoAPI.Models.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class User : XI_Modify
    {
        [Required]
        public int RoleId { get; set; }
        
        public int? TableId { get; set; }

        [Required, MaxLength(20)]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
        
        [Required, MaxLength(50)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
        
        public bool IsTable { get; set; }

        [ForeignKey(nameof(RoleId))]
        public virtual Role Role { get; set; }

        [ForeignKey(nameof(TableId))]
        public virtual Table Table { get; set; }
    }
}
