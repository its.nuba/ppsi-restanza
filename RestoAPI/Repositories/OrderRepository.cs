﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace RestoAPI.Repositories
{
    using Models;
    using Models.DB;
    using Models.Ext;

    public class OrderRepository
    {
        private RestoContext _context;

        public OrderRepository(RestoContext context)
        {
            _context = context;
        }

        #region table

        public List<TableWithStatus> GetTableList(bool includeFinish = false)
        {
            var result = new List<TableWithStatus>();
            var table = _context.Table.ToList();
            foreach(var item in table)
            {
                var order = _context.Order.Where(x => x.TableId == item.Id && (includeFinish || x.Status != Order.OrderStatus.Finished));

                var orderitem = order.FirstOrDefault();
                var status = order.Any() ? order.OrderByDescending(x => x.CheckInOn).FirstOrDefault().Status : Order.OrderStatus.Available;
                var totalorder = order.SelectMany(x => x.OrderDetail.Where(y => y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).FirstOrDefault().Status != OrderDetailStatus.StatusType.Canceled)).Count();
                var totaldeliver = order.SelectMany(x => x.OrderDetail.Where(y => y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).FirstOrDefault().Status == OrderDetailStatus.StatusType.Delivered)).Count();

                var resultItem = new TableWithStatus {
                    Id = item.Id,
                    Name = item.Name,
                    Capacity = item.Capacity,
                    Status = status,
                    StatusText = status.ToString(),
                    FinishedOrder = totaldeliver,
                    TotalOrder = totalorder,
                    TotalAmount = orderitem != null ? orderitem.TotalAmount : 0,
                    TotalTax = orderitem != null ? orderitem.TotalTax : 0,
                    GrandTotal = orderitem != null ? orderitem.TotalAmount + orderitem.TotalTax : 0
                };

                result.Add(resultItem);
            }

            return result;
        }

        public TableWithStatus GetTableLastStatus(int idTable, bool includeFinish = false)
        {
            var table = _context.Table.Where(x => x.Id == idTable).FirstOrDefault();
            var order = _context.Order.Where(x => x.TableId == table.Id && (includeFinish || x.Status != Order.OrderStatus.Finished));

            var orderitem = order.FirstOrDefault();
            var status = order.Any() ? order.OrderByDescending(x => x.CheckInOn).FirstOrDefault().Status : Order.OrderStatus.Available;
            var totalorder = order.SelectMany(x => x.OrderDetail.Where(y => y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).FirstOrDefault().Status != OrderDetailStatus.StatusType.Canceled)).Count();
            var totaldeliver = order.SelectMany(x => x.OrderDetail.Where(y => y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).FirstOrDefault().Status == OrderDetailStatus.StatusType.Delivered)).Count();

            var resultItem = new TableWithStatus
            {
                Id = table.Id,
                Name = table.Name,
                Capacity = table.Capacity,
                Status = status,
                StatusText = status.ToString(),
                FinishedOrder = totaldeliver,
                TotalOrder = totalorder,
                TotalAmount = orderitem != null ? orderitem.TotalAmount : 0,
                TotalTax = orderitem != null ? orderitem.TotalTax : 0,
                GrandTotal = orderitem != null ? orderitem.TotalAmount + orderitem.TotalTax : 0
            };
            
            return resultItem;
        }

        public List<OrderWithStatus> GetTableOrderList(int idTable, bool allFlag = false)
        {
            var result = _context.Order.Where(x => x.TableId == idTable).OrderByDescending(x => x.CheckInOn)
                .Select(x => new OrderWithStatus
                {
                    CheckInOn = x.CheckInOn,
                    CheckOutOn = x.CheckOutOn,
                    Id = x.Id,
                    Status = x.Status,
                    StatusText = x.Status.ToString(),
                    TableId = x.TableId,
                    TotalAmount = x.TotalAmount,
                    TotalTax = x.TotalTax,
                    OrderDetail = x.OrderDetail.Where(y => allFlag || !y.OrderDetailStatus.Any(z => z.Status == OrderDetailStatus.StatusType.Canceled)).Select(y => new OrderDetailWithStatus
                    {
                        Id = y.Id,
                        OrderId = y.OrderId,
                        CreatedBy = y.CreatedBy,
                        CreatedOn = y.CreatedOn,
                        LastStatus = y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).Select(z => z.Status).FirstOrDefault(),
                        LastStatusText = y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).Select(z => z.Status).FirstOrDefault().ToString(),
                        MenuId = y.MenuId,
                        Menu = y.Menu.Name,
                        MenuPrice = y.Menu.Price,
                        TableId = y.Order.TableId,
                        TableName = y.Order.Table.Name,
                        Note = y.Note,
                        TotalQuantity = y.TotalQuantity,
                        UpdateBy = y.UpdateBy,
                        UpdateOn = y.UpdateOn
                    }).ToList()
                }).ToList();
             
            return result;
        }

        public OrderWithStatus GetTableOrderLast(int idTable, bool allFlag = false)
        {
            var result = _context.Order.Where(x => x.TableId == idTable).OrderByDescending(x => x.CheckInOn)
                .Select(x => new OrderWithStatus
                {
                    CheckInOn = x.CheckInOn,
                    CheckOutOn = x.CheckOutOn,
                    Id = x.Id,
                    Status = x.Status,
                    StatusText = x.Status.ToString(),
                    TableId = x.TableId,
                    TotalAmount = x.TotalAmount,
                    TotalTax = x.TotalTax,
                    OrderDetail = x.OrderDetail.Where(y => allFlag || !y.OrderDetailStatus.Any(z => z.Status == OrderDetailStatus.StatusType.Canceled)).Select(y => new OrderDetailWithStatus
                    {
                        Id = y.Id,
                        OrderId = y.OrderId,
                        CreatedBy = y.CreatedBy,
                        CreatedOn = y.CreatedOn,
                        LastStatus = y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).Select(z => z.Status).FirstOrDefault(),
                        LastStatusText = y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).Select(z => z.Status).FirstOrDefault().ToString(),
                        MenuId = y.MenuId,
                        Menu = y.Menu.Name,
                        MenuPrice = y.Menu.Price,
                        TableId = y.Order.TableId,
                        TableName = y.Order.Table.Name,
                        Note = y.Note,
                        TotalQuantity = y.TotalQuantity,
                        UpdateBy = y.UpdateBy,
                        UpdateOn = y.UpdateOn
                    }).ToList()
                }).FirstOrDefault();

            return result;
        }

        #endregion

        #region order 

        public List<KeyValuePair<int, string>> GetOrderStatusList()
        {
            var result = new List<KeyValuePair<int, string>>();
            foreach (int item in Enum.GetValues(typeof(Order.OrderStatus)))
            {
                var nitem = new KeyValuePair<int, string>(item, ((Order.OrderStatus)item).ToString());
                result.Add(nitem);
            }

            return result;
        }

        public Order GetOrder(int id)
        {
            var result = _context.Order.Where(x => x.Id == id).FirstOrDefault();

            return result;
        }

        public int AddOrder(Order data)
        {
            if (data != null)
            {
                _context.Add(data);
                _context.SaveChanges();

                return data.Id;
            }

            return 0;
        }

        public int EditOrder(Order data)
        {
            if (data != null)
            {
                var exist = GetOrder(data.Id);
                if (exist != null)
                {
                    _context.Entry(exist).CurrentValues.SetValues(data);
                    _context.SaveChanges();

                    return data.Id;
                }
            }
            return 0;
        }

        #endregion

        #region order detail

        public List<OrderDetailWithStatus> GetOrderDetailListWithStatus(int idOrder, bool allFlag = false)
        {
            var result = _context.OrderDetail.Where(y => y.OrderId == idOrder && (allFlag || !y.OrderDetailStatus.Any(z => z.Status == OrderDetailStatus.StatusType.Canceled))).Select(y => new OrderDetailWithStatus
            {
                Id = y.Id,
                OrderId = y.OrderId,
                CreatedBy = y.CreatedBy,
                CreatedOn = y.CreatedOn,
                LastStatus = y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).Select(z => z.Status).FirstOrDefault(),
                LastStatusText = y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).Select(z => z.Status).FirstOrDefault().ToString(),
                LastStatusTime = y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).Select(z => z.CreatedOn).FirstOrDefault(),
                MenuId = y.MenuId,
                Menu = y.Menu.Name,
                MenuPrice = y.Menu.Price,
                TableId = y.Order.TableId,
                TableName = y.Order.Table.Name,
                Note = y.Note,
                TotalQuantity = y.TotalQuantity,
                UpdateBy = y.UpdateBy,
                UpdateOn = y.UpdateOn
            }).ToList();

            return result;
        }

        public OrderDetailWithStatus GetOrderDetailWithStatus(int idOrderDetail)
        {
            var result = _context.OrderDetail.Where(y => y.Id == idOrderDetail).Select(y => new OrderDetailWithStatus
            {
                Id = y.Id,
                OrderId = y.OrderId,
                CreatedBy = y.CreatedBy,
                CreatedOn = y.CreatedOn,
                LastStatus = y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).Select(z => z.Status).FirstOrDefault(),
                LastStatusText = y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).Select(z => z.Status).FirstOrDefault().ToString(),
                LastStatusTime = y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).Select(z => z.CreatedOn).FirstOrDefault(),
                MenuId = y.MenuId,
                Menu = y.Menu.Name,
                MenuPrice = y.Menu.Price,
                Note = y.Note,
                TotalQuantity = y.TotalQuantity,
                UpdateBy = y.UpdateBy,
                UpdateOn = y.UpdateOn
            }).FirstOrDefault();

            return result;
        }

        public List<OrderDetail> GetOrderDetailList(int idOrder)
        {
            var result = _context.OrderDetail.Where(x => x.OrderId == idOrder).ToList();

            return result;
        }

        public OrderDetail GetOrderDetail(int id)
        {
            var result = _context.OrderDetail.Where(x => x.Id == id).FirstOrDefault();

            return result;
        }

        public int AddOrderDetail(OrderDetail data)
        {
            if (data != null)
            {
                var order = GetOrder(data.OrderId);
                if (order.Status != Order.OrderStatus.Completed)
                {
                    _context.Add(data);
                    _context.SaveChanges();

                    return data.Id;
                }
            }

            return 0;
        }

        public int EditOrderDetail(OrderDetail data)
        {
            if (data != null)
            {
                var exist = GetOrderDetail(data.Id);
                if (exist != null)
                {
                    var order = GetOrder(data.OrderId);
                    if (order.Status != Order.OrderStatus.Completed)
                    {
                        _context.Entry(exist).CurrentValues.SetValues(data);
                        _context.SaveChanges();

                        return data.Id;
                    }
                }
            }
            return 0;
        }

        #endregion

        #region order detail status

        public List<KeyValuePair<int, string>> GetOrderDetailStatusList()
        {
            var result = new List<KeyValuePair<int, string>>();
            foreach (int item in Enum.GetValues(typeof(OrderDetailStatus.StatusType)))
            {
                var nitem = new KeyValuePair<int, string>(item, ((OrderDetailStatus.StatusType)item).ToString());
                result.Add(nitem);
            }

            return result;
        }

        public OrderDetailStatus GetLastOrderDetailStatus(int idOrderDetail)
        {
            var result = _context.OrderDetailStatus.Where(x => x.OrderDetailId == idOrderDetail).OrderByDescending(x => x.CreatedOn).FirstOrDefault();

            return result;
        }

        public int AddOrderDetailStatus(OrderDetailStatus data)
        {
            if (data != null)
            {
                var orderDetail = GetOrderDetail(data.OrderDetailId);
                if (orderDetail != null)
                {
                    _context.Add(data);
                    _context.SaveChanges();

                    return data.Id;
                }
            }

            return 0;
        }

        #endregion

        public void DeleteTransactionalData()
        {
            // order status
            var allStatus = _context.OrderDetailStatus.ToList();
            _context.RemoveRange(allStatus);
            _context.SaveChanges();

            // order detail  
            var allDetail = _context.OrderDetail.ToList();
            _context.RemoveRange(allDetail);
            _context.SaveChanges();

            // order
            var allOrder = _context.Order.ToList();
            _context.RemoveRange(allOrder);
            _context.SaveChanges();
        }

        public void InsertTransactionalData()
        {
            var table = _context.Table.ToList();
            foreach (var item in table)
            {
                _context.Add(new Order { CheckInOn = DateTime.Now, TableId = item.Id, Status = Order.OrderStatus.Waiting });
            }
            _context.SaveChanges();

            var order = _context.Order.ToList();
            var menu = _context.Menu.ToList();
            foreach (var item in order)
            {
                foreach (var itemm in menu)
                {
                    _context.Add(new OrderDetail { CreatedOn = DateTime.Now, MenuId = itemm.Id, Note = "No note", OrderId = item.Id, TotalQuantity = 1 });
                    _context.SaveChanges();

                    var om = _context.OrderDetail.LastOrDefault();
                    _context.Add(new OrderDetailStatus { CreatedOn = DateTime.Now, OrderDetailId = om.Id, Status = OrderDetailStatus.StatusType.Queue });
                }
            }
        }

        public List<OrderDetailWithStatus> GetAllOrderDetailForToday()
        {
            var yesterday = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59).AddDays(-1);

            var result = (from o in _context.Order.Where(x => x.CheckInOn > yesterday)
                          join od in _context.OrderDetail on o.Id equals od.OrderId
                          join m in _context.Menu on od.MenuId equals m.Id
                          join t in _context.Table on o.TableId equals t.Id
                          let os = _context.OrderDetailStatus.Where(x => x.OrderDetailId == od.Id).OrderByDescending(z => z.CreatedOn).FirstOrDefault()
                          select new OrderDetailWithStatus
                          {
                              Id = od.Id,
                              OrderId = od.OrderId,
                              CreatedBy = od.CreatedBy,
                              CreatedOn = od.CreatedOn,
                              LastStatus = os != null ? os.Status : OrderDetailStatus.StatusType.Queue,
                              LastStatusText = (os != null ? os.Status : OrderDetailStatus.StatusType.Queue).ToString(),
                              LastStatusTime = (os != null ? os.CreatedOn : DateTime.Now),
                              MenuId = od.MenuId,
                              Menu = m.Name,
                              MenuPrice = m.Price,
                              TableId = t.Id,
                              TableName = t.Name,
                              Note = od.Note,
                              TotalQuantity = od.TotalQuantity,
                              UpdateBy = od.UpdateBy,
                              UpdateOn = od.UpdateOn
                          }).OrderBy(x => x.LastStatus == OrderDetailStatus.StatusType.Progress ? -1 : (int)x.LastStatus).ThenBy(x => x.LastStatusTime).ToList();

            //var result = _context.Order.Where(x => x.CheckInOn > yesterday)
            //    .SelectMany(x => x.OrderDetail
            //        .Select(y => new OrderDetailWithStatus
            //        {
            //            Id = y.Id,
            //            OrderId = y.OrderId,
            //            CreatedBy = y.CreatedBy,
            //            CreatedOn = y.CreatedOn,
            //            LastStatus = y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).Select(z => z.Status).FirstOrDefault(),
            //            LastStatusText = y.OrderDetailStatus.OrderByDescending(z => z.CreatedOn).Select(z => z.Status).FirstOrDefault().ToString(),
            //            MenuId = y.MenuId,
            //            Menu = y.Menu.Name,
            //            MenuPrice = y.Menu.Price,
            //            Note = y.Note,
            //            TotalQuantity = y.TotalQuantity,
            //            UpdateBy = y.UpdateBy,
            //            UpdateOn = y.UpdateOn
            //        }).ToList()
            //    ).ToList();

            return result;
        }
    }
}
