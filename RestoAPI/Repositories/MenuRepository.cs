﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace RestoAPI.Repositories
{
    using Models;
    using Models.DB;
    using Models.Ext;

    public class MenuRepository
    {
        private RestoContext _context;
        private string WebBaseUrl;

        public MenuRepository(RestoContext context, IConfiguration configuration)
        {
            _context = context;

            WebBaseUrl = configuration["WebBaseUrl"];
        }

        #region menu

        #region CRUD

        public IEnumerable<Menu> GetMenuList()
        {
            var result = _context.Menu.ToList();

            result.ForEach(x => x.Picture = WebBaseUrl + x.Picture);

            return result;
        }
        public Menu GetMenu(int id, bool withBaseUrl = false)
        {
            var result = _context.Menu.Where(x => x.Id == id).FirstOrDefault();
            if (result != null) result.Picture = (withBaseUrl ? WebBaseUrl : "") + result.Picture;

            return result;
        }
        public int AddMenu(Menu data)
        {
            if (data != null)
            {
                var exist = _context.Menu.Where(x => x.Name == data.Name).Any();
                if (!exist)
                {
                    _context.Add(data);
                    _context.SaveChanges();

                    return data.Id;
                }
            }

            return 0;
        }
        public int EditMenu(Menu data)
        {
            if (data != null)
            {
                var exist = GetMenu(data.Id);
                if (exist != null)
                {
                    var same = _context.Menu.Where(x => x.Id != data.Id && x.Name == data.Name).Any();
                    if (!same)
                    {
                        _context.Entry(exist).CurrentValues.SetValues(data);
                        _context.SaveChanges();

                        return data.Id;
                    }
                }
            }
            return 0;
        }
        public bool DeleteMenu(int id)
        {
            var exist = GetMenu(id);
            if (exist != null)
            {
                _context.Remove(exist);
                _context.SaveChanges();

                return true;
            }

            return false;
        }

        #endregion

        public List<KeyValuePair<int, string>> GetFoodTypeList()
        {
            var result = new List<KeyValuePair<int, string>>();
            foreach (int item in Enum.GetValues(typeof(Menu.FoodTypes)))
            {
                var nitem = new KeyValuePair<int, string>(item, ((Menu.FoodTypes)item).ToString());
                result.Add(nitem);
            }

            return result;
        }

        public MenuWithType GetMenuWithType(int id)
        {
            var result = _context.Menu.Where(x => x.Id == id)
                .Select(x => new MenuWithType
                {
                    Id = x.Id,
                    CreatedBy = x.CreatedBy,
                    CreatedOn = x.CreatedOn,
                    Description = x.Description,
                    FoodType = x.FoodType.ToString(),
                    IsActive = x.IsActive,
                    IsRecomended = x.IsRecomended,
                    Name = x.Name,
                    Picture = WebBaseUrl + x.Picture,
                    Price = x.Price,
                    UpdateBy = x.UpdateBy,
                    UpdateOn = x.UpdateOn

                }).FirstOrDefault();

            return result;
        }

        public IEnumerable<MenuWithType> GetMenuWithTypeList(string types = "")
        {
            var sucessType = Int32.TryParse(types, out int typesid) && !string.IsNullOrEmpty(types);

            var result = _context.Menu.Where(x => sucessType ? (int)x.FoodType == typesid : (!string.IsNullOrEmpty(types) ? x.FoodType.ToString() == types : true))
                .Select(x => new MenuWithType {
                    Id = x.Id,
                    CreatedBy = x.CreatedBy,
                    CreatedOn = x.CreatedOn,
                    Description = x.Description,
                    FoodType = x.FoodType.ToString(),
                    IsActive = x.IsActive,
                    IsRecomended = x.IsRecomended,
                    Name = x.Name,
                    Picture = WebBaseUrl + x.Picture,
                    Price = x.Price,
                    UpdateBy = x.UpdateBy,
                    UpdateOn = x.UpdateOn
                    
                });

            return result.ToList();
        }

        #endregion
    }
}
