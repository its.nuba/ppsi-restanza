﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoAPI.Repositories
{
    using Helpers;
    using Models;
    using Models.DB;
    using Models.Ext;

    public class UserRepository
    {
        private RestoContext _context;

        public UserRepository(RestoContext context)
        {
            _context = context;
        }

        #region user

        #region CRUD

        public IEnumerable<User> GetUserList()
        {
            return _context.User.ToList();
        }
        public User GetUser(int id)
        {
            return _context.User.Where(x => x.Id == id).FirstOrDefault();
        }
        public User GetUser(string usernameorid)
        {
            var user = _context.User.Where(x => x.Username == usernameorid).FirstOrDefault();

            if (user == null)
            {
                int id = 0;
                int.TryParse(usernameorid, out id);
                user = GetUser(id);
            }

            return user;
        }
        public int AddUser(User data)
        {
            if (data != null)
            {
                var exist = _context.User.Where(x => x.Username == data.Username).Any();
                if (!exist)
                {
                    _context.Add(data);
                    _context.SaveChanges();

                    return data.Id;
                }
            }

            return 0;
        }
        public int EditUser(User data)
        {
            if (data != null)
            {
                var exist = GetUser(data.Id);
                if (exist != null)
                {
                    var same = _context.User.Where(x => x.Id != data.Id && x.Username == data.Username).Any();
                    if (!same)
                    {
                        _context.Entry(exist).CurrentValues.SetValues(data);
                        _context.SaveChanges();

                        return data.Id;
                    }
                }
            }
            return 0;
        }
        public bool DeleteUser(int id)
        {
            var exist = GetUser(id);
            if (exist != null)
            {
                _context.Remove(exist);
                _context.SaveChanges();

                return true;
            }

            return false;
        }

        #endregion

        public bool CheckUsernamePassword(string username, string password)
        {
            var hashedPassword = Encryption.GetHash(password);

            return _context.User.Where(x => x.Username == username && x.Password == Encryption.GetHash(password) && x.IsActive).Any();
        }
        
        public IEnumerable<UserWithRole> GetUserOnlyList()
        {
            var result = _context.User.Where(x => !x.IsTable).Select(x => new UserWithRole { user = x, role = x.Role }).ToList();

            return result;
        }

        #endregion

        #region role
        
        public IEnumerable<Role> GetRoleList()
        {
            return _context.Role.ToList();
        }
        public Role GetRole(int id)
        {
            return _context.Role.Where(x => x.Id == id).FirstOrDefault();
        }

        #endregion

        #region Role Access

        public IEnumerable<RoleAccessList> GetRoleAccessList(int id)
        {
            return _context.RoleAccess.Where(x => x.RoleId == id).Select(x => new RoleAccessList { idRole = x.RoleId, type = x.Access.Type, module = x.Access.Module }).ToList();
        }

        #endregion

        #region table

        #region CRUD

        public IEnumerable<Table> GetTableList()
        {
            return _context.Table.ToList();
        }
        public Table GetTable(int id)
        {
            return _context.Table.Where(x => x.Id == id).FirstOrDefault();
        }
        public int AddTable(Table data)
        {
            if (data != null)
            {
                var exist = _context.Table.Where(x => x.Name == data.Name).Any();
                if (!exist)
                {
                    _context.Add(data);
                    _context.SaveChanges();

                    return data.Id;
                }
            }

            return 0;
        }
        public int EditTable(Table data)
        {
            if (data != null)
            {
                var exist = GetTable(data.Id);
                if (exist != null)
                {
                    var same = _context.Table.Where(x => x.Id != data.Id && x.Name == data.Name).Any();
                    if (!same)
                    {
                        _context.Entry(exist).CurrentValues.SetValues(data);
                        _context.SaveChanges();

                        return data.Id;
                    }
                }
            }
            return 0;
        }
        public bool DeleteTable(int id)
        {
            var exist = GetUser(id);
            if (exist != null)
            {
                _context.Remove(exist);
                _context.SaveChanges();

                return true;
            }

            return false;
        }

        #endregion
        
        public IEnumerable<UserWithTable> GetUserTableList()
        {
            var result = _context.User.Where(x => x.IsTable).Select(x => new UserWithTable { user = x, table = x.Table }).ToList();

            return result;
        }
        
        public UserWithTable GetUserTable(int id)
        {
            var result = _context.User.Where(x => x.IsTable && x.Id == id).Select(x => new UserWithTable { user = x, table = x.Table }).FirstOrDefault();

            return result;
        }

        public UserWithTable GetUserByTable(int idTable)
        {
            var result = _context.User.Where(x => x.IsTable && x.TableId == idTable).Select(x => new UserWithTable { user = x, table = x.Table }).FirstOrDefault();

            return result;
        }

        #endregion

    }
}
