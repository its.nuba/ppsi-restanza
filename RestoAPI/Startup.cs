﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;

namespace RestoAPI
{
    using Models;
    using Repositories;

    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add database connection
            var apiConnection = Configuration.GetConnectionString("resto_db");
            services.AddDbContext<RestoContext>(options => options.UseSqlServer(apiConnection));

            services.AddCors(options => {
                options.AddPolicy("CORS Policy",
                    builder => builder.AllowAnyOrigin());
            });

            // Add framework services.
            services.AddMvc();

            // Add configuration to be available in all class
            services.AddSingleton<IConfiguration>(Configuration);

            //Add repositories
            services.AddScoped<UserRepository, UserRepository>();
            services.AddScoped<MenuRepository, MenuRepository>();
            services.AddScoped<OrderRepository, OrderRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, RestoContext context)
        {
            RestoContextInitializer.Initializer(context);
            
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseCors("CORS Policy");

            app.UseMvc();
        }
    }
}
