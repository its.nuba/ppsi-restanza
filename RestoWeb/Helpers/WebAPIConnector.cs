﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RestoWeb.Helpers
{
    using Models;

    public class WebAPIConnector
    {
        private static HttpClient _client = new HttpClient();
        private static string _baseUrl;

        public WebAPIConnector(IConfiguration configuration)
        {
            _baseUrl = configuration["APIBaseUrl"];
        }

        private APIResult<JObject> SendGet(string api_url, bool isDel = false)
        {
            var response = isDel ? _client.DeleteAsync(_baseUrl + api_url).Result : _client.GetAsync(_baseUrl + api_url).Result;

            var result = new APIResult<JObject>(false, "Cannot connect to API");
            if (response.IsSuccessStatusCode)
            {
                var json = response.Content.ReadAsStringAsync().Result;

                result = JsonConvert.DeserializeObject<APIResult<JObject>>(json);
            }

            return result;
        }

        public APIResult<T> SendGet<T>(string api_url)
        {
            APIResult<JObject> temp = SendGet(api_url);

            var result = new APIResult<T>(temp.status, temp.message);
            if (temp.data != null) result.data = temp.data.ToObject<T>();

            return result;
        }

        public APIResult<T> SendDelete<T>(string api_url)
        {
            APIResult<JObject> temp = SendGet(api_url, true);

            var result = new APIResult<T>(temp.status, temp.message);
            if (temp.data != null) result.data = temp.data.ToObject<T>();

            return result;
        }

        private APIResult<JArray> SendGetList(string api_url)
        {
            var response = _client.GetAsync(_baseUrl + api_url).Result;

            var result = new APIResult<JArray>(false, "Cannot connect to API");
            if (response.IsSuccessStatusCode)
            {
                var json = response.Content.ReadAsStringAsync().Result;

                result = JsonConvert.DeserializeObject<APIResult<JArray>>(json);
            }

            return result;
        }

        public APIResult<List<T>> SendGetList<T>(string api_url)
        {
            APIResult<JArray> temp = SendGetList(api_url);

            var result = new APIResult<List<T>>(temp.status, temp.message);
            if (temp.data != null) result.data = temp.data.ToObject<List<T>>();

            return result;
        }

        private APIResult<JObject> SendPost(string api_url, dynamic param, bool isPut = false)
        {
            // setup parameters
            JObject o = JObject.FromObject(param);

            // setup request
            var content = new StringContent(o.ToString());
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            // send request
            var response = isPut ? _client.PutAsync(_baseUrl + api_url, content).Result : _client.PostAsync(_baseUrl + api_url, content).Result;

            // read response
            var result = new APIResult<JObject>(false, "Cannot connect to API");
            if (response.IsSuccessStatusCode)
            {
                var json = response.Content.ReadAsStringAsync().Result;

                result = JsonConvert.DeserializeObject<APIResult<JObject>>(json);
            }

            return result;
        }

        public APIResult<T> SendPost<T>(string api_url, dynamic param)
        {
            APIResult<JObject> temp = SendPost(api_url, param);

            var result = new APIResult<T>(temp.status, temp.message);
            if (temp.data != null) result.data = temp.data.ToObject<T>();

            return result;
        }

        public APIResult<T> SendPut<T>(string api_url, dynamic param)
        {
            APIResult<JObject> temp = SendPost(api_url, param, true);

            var result = new APIResult<T>(temp.status, temp.message);
            if (temp.data != null) result.data = temp.data.ToObject<T>();

            return result;
        }
    }
}
