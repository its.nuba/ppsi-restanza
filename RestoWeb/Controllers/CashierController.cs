using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace RestoWeb.Controllers
{
    using Helpers;

    [Authorize]
    public class CashierController : BaseController
    {
        private readonly WebAPIConnector _api;

        public CashierController(WebAPIConnector api)
        {
            _api = api;
        }

        public IActionResult Index()
        {
            var response = _api.SendGetList<RestoAPI.Models.Ext.TableWithStatus>("table");

            return View(response.data);
        }

        public IActionResult GetTable(int id, bool all)
        {
            var response = _api.SendGet<RestoAPI.Models.Ext.OrderWithStatus>("table/" + id + "/lastorder" + (all ? "/all" : ""));

            return View(response.data);
        }

        public bool UpdateStatus(int id, RestoAPI.Models.DB.Order.OrderStatus status)
        {
            var iduser = Convert.ToInt32(User.FindFirst(ClaimTypes.Sid).Value);

            var response = _api.SendPut<RestoAPI.Models.Ext.OrderDetailWithStatus>("order", new { id = id, status = status, createdby = iduser });

            if (!response.status)
            {
                TempData["msg-color"] = response.status ? "success" : "warning";
                TempData["msg"] = response.message;
            }

            if (response.status)
            {
                return true;
            }

            return false;
        }
    }
}