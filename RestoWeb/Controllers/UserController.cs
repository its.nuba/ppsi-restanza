using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace RestoWeb.Controllers
{
    using Helpers;

    [Authorize]
    public class UserController : BaseController
    {
        private readonly WebAPIConnector _api;
        private readonly List<RestoAPI.Models.DB.Role> _roles;

        public UserController(WebAPIConnector api)
        {
            _api = api;

            _roles = _api.SendGetList<RestoAPI.Models.DB.Role>("role").data.Where(x => x.Name != "Table").ToList();
        }

        public IActionResult Index()
        {
            var response = _api.SendGetList<RestoAPI.Models.Ext.UserWithRole>("user/WithRole");

            return View(response.data);
        }

        public IActionResult Add()
        {
            ViewData["Roles"] = _roles;

            return View("Edit", new RestoAPI.Models.DB.User());
        }

        [HttpPost]
        public IActionResult Add(RestoAPI.Models.DB.User data)
        {
            var id = Convert.ToInt32(User.FindFirst(ClaimTypes.Sid).Value);

            // lengkapi data
            data.CreatedBy = id;
            data.CreatedOn = DateTime.Now;
            data.IsTable = false;

            var response = _api.SendPost<RestoAPI.Models.DB.User>("user", data);

            TempData["msg-color"] = response.status ? "success" : "danger";
            TempData["msg"] = response.message;

            if (response.status)
            {
                return RedirectToAction("Index");
            }

            ViewData["Roles"] = _roles;
            return View("Edit", data);
        }

        public IActionResult Edit(int id)
        {
            var response = _api.SendGet<RestoAPI.Models.DB.User>("user/" + id);

            if (!response.status)
            {
                TempData["msg-color"] = response.status ? "success" : "warning";
                TempData["msg"] = response.message;
            }

            if (response.status)
            {
                ViewData["Roles"] = _roles;

                return View(response.data);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Edit(RestoAPI.Models.DB.User data)
        {
            var id = Convert.ToInt32(User.FindFirst(ClaimTypes.Sid).Value);

            // lengkapi data
            data.UpdateBy = id;
            data.UpdateOn = DateTime.Now;
            data.IsTable = false;

            var response = _api.SendPut<RestoAPI.Models.DB.User>("user", data);
            
            TempData["msg-color"] = response.status ? "success" : "danger";
            TempData["msg"] = response.message;
            
            if (response.status)
            {
                return RedirectToAction("Index");
            }

            ViewData["Roles"] = _roles;
            return RedirectToAction("Edit", new { id = data.Id });
        }

        public IActionResult Delete(int id)
        {
            var response = _api.SendDelete<RestoAPI.Models.DB.User>("user/" + id);
            
            TempData["msg-color"] = response.status ? "success" : "warning";
            TempData["msg"] = response.message;
            
            return RedirectToAction("Index");
        }
    }
}