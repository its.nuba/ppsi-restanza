using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace RestoWeb.Controllers
{
    using Helpers;

    [Authorize]
    public class TableController : BaseController
    {
        private readonly WebAPIConnector _api;
        private readonly int _idTableRole;

        public TableController(WebAPIConnector api)
        {
            _api = api;

            var roles = _api.SendGetList<RestoAPI.Models.DB.Role>("role").data;
            _idTableRole = roles.Where(x => x.Name == "Table").FirstOrDefault().Id;
        }

        public IActionResult Index()
        {
            var response = _api.SendGetList<RestoAPI.Models.Ext.UserWithTable>("user/WithTable");

            return View(response.data);
        }

        public IActionResult Add()
        {
            return View("Edit", new RestoAPI.Models.Ext.UserWithTable { user = new RestoAPI.Models.DB.User(), table = new RestoAPI.Models.DB.Table() });
        }

        [HttpPost]
        public IActionResult Add(RestoAPI.Models.DB.User data, int Capacity)
        {
            var id = Convert.ToInt32(User.FindFirst(ClaimTypes.Sid).Value);

            // insert table first
            var table = new RestoAPI.Models.DB.Table { Name = data.Name, Capacity = Capacity };
            var responseTable = _api.SendPost<RestoAPI.Models.DB.Table>("table", table);

            if (responseTable.status)
            {
                // lengkapi data
                data.CreatedBy = id;
                data.CreatedOn = DateTime.Now;
                data.IsTable = true;
                data.RoleId = _idTableRole;
                data.TableId = responseTable.data.Id;

                var response = _api.SendPost<RestoAPI.Models.DB.User>("user", data);

                TempData["msg-color"] = response.status ? "success" : "danger";
                TempData["msg"] = response.message;

                if (response.status)
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["msg-color"] = responseTable.status ? "success" : "danger";
                TempData["msg"] = responseTable.message;
            }

            return View("Edit", new RestoAPI.Models.Ext.UserWithTable { user = data, table = new RestoAPI.Models.DB.Table { Capacity = Capacity } });
        }

        public IActionResult Edit(int id)
        {
            var response = _api.SendGet<RestoAPI.Models.Ext.UserWithTable>("user/" + id + "/WithTable");

            if (!response.status)
            {
                TempData["msg-color"] = response.status ? "success" : "warning";
                TempData["msg"] = response.message;
            }

            if (response.status)
            {
                return View(response.data);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Edit(RestoAPI.Models.DB.User data, int Capacity)
        {
            var id = Convert.ToInt32(User.FindFirst(ClaimTypes.Sid).Value);

            // lengkapi data
            data.UpdateBy = id;
            data.UpdateOn = DateTime.Now;
            data.IsTable = true;
            data.RoleId = _idTableRole;

            var response = _api.SendPut<RestoAPI.Models.DB.User>("user", data);
            
            TempData["msg-color"] = response.status ? "success" : "danger";
            TempData["msg"] = response.message;
            
            if (response.status)
            {
                // update table
                var table = _api.SendGet<RestoAPI.Models.Ext.UserWithTable>("user/" + data.Id + "/WithTable");
                table.data.table.Name = data.Name;
                table.data.table.Capacity = Capacity;
                var responseTable = _api.SendPut<RestoAPI.Models.DB.Table>("table", table.data.table);

                return RedirectToAction("Index");
            }
            
            return RedirectToAction("Edit", new { id = data.Id });
        }

        public IActionResult Delete(int id)
        {
            var response = _api.SendDelete<RestoAPI.Models.DB.User>("user/" + id);
            
            TempData["msg-color"] = response.status ? "success" : "warning";
            TempData["msg"] = response.message;
            
            return RedirectToAction("Index");
        }
    }
}