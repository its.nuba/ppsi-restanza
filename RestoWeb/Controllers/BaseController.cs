﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;

namespace RestoWeb.Controllers
{
    public class BaseController : Controller
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            ViewData["username"] = User.FindFirst(ClaimTypes.Name).Value;
            ViewData["roleid"] = User.FindFirst(ClaimTypes.Role).Value;

            base.OnActionExecuting(context);
        }
    }
}
