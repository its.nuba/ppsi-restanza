using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace RestoWeb.Controllers
{
    using Helpers;

    [Authorize]
    public class MenuController : BaseController
    {
        private readonly WebAPIConnector _api;
        private readonly List<KeyValuePair<int, string>> _foodType;

        public MenuController(WebAPIConnector api)
        {
            _api = api;

            _foodType = _api.SendGetList<KeyValuePair<int, string>>("menu/foodtype").data;
        }

        public IActionResult Index()
        {
            var response = _api.SendGetList<RestoAPI.Models.DB.Menu>("menu");

            return View(response.data);
        }

        public IActionResult Add()
        {
            ViewData["Types"] = _foodType;

            return View("Edit", new RestoAPI.Models.DB.Menu());
        }

        [HttpPost]
        public IActionResult Add(RestoAPI.Models.DB.Menu data, int Type, IFormFile Image)
        {
            var id = Convert.ToInt32(User.FindFirst(ClaimTypes.Sid).Value);

            // lengkapi data
            data.CreatedBy = id;
            data.CreatedOn = DateTime.Now;
            data.FoodType = (RestoAPI.Models.DB.Menu.FoodTypes)Type;
            if (Image != null && Image.Name != null)
            {
                data.Picture = UpdateImage(Image);
            }

            var response = _api.SendPost<RestoAPI.Models.DB.Menu>("menu", data);

            TempData["msg-color"] = response.status ? "success" : "danger";
            TempData["msg"] = response.message;

            if (response.status)
            {
                return RedirectToAction("Index");
            }

            ViewData["Types"] = _foodType;
            return View("Edit", data);
        }

        public IActionResult Edit(int id)
        {
            var response = _api.SendGet<RestoAPI.Models.DB.Menu>("menu/" + id);

            if (!response.status)
            {
                TempData["msg-color"] = response.status ? "success" : "warning";
                TempData["msg"] = response.message;
            }

            if (response.status)
            {
                ViewData["Types"] = _foodType;

                return View(response.data);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Edit(RestoAPI.Models.DB.Menu data, int Type, IFormFile Image)
        {
            var id = Convert.ToInt32(User.FindFirst(ClaimTypes.Sid).Value);

            // lengkapi data
            data.UpdateBy = id;
            data.UpdateOn = DateTime.Now;
            data.FoodType = (RestoAPI.Models.DB.Menu.FoodTypes)Type;
            if (Image != null && Image.Name != null)
            {
                data.Picture = UpdateImage(Image);
            }

            var response = _api.SendPut<RestoAPI.Models.DB.Menu>("menu", data);

            TempData["msg-color"] = response.status ? "success" : "danger";
            TempData["msg"] = response.message;

            if (response.status)
            {
                return RedirectToAction("Index");
            }

            ViewData["Types"] = _foodType;
            return RedirectToAction("Edit", new { id = data.Id });
        }

        public IActionResult Delete(int id)
        {
            var response = _api.SendDelete<RestoAPI.Models.DB.Menu>("menu/" + id);

            TempData["msg-color"] = response.status ? "success" : "warning";
            TempData["msg"] = response.message;

            return RedirectToAction("Index");
        }

        private string UpdateImage(IFormFile Image)
        {
            var ext = Image.FileName.Split('.').LastOrDefault();
            var newFile = "menu/" + DateTime.Now.ToString("yyyyMMddHHss") + "." + ext;

            Directory.CreateDirectory(Path.GetFullPath("wwwroot/menu"));
            using (FileStream st = new FileStream(Path.GetFullPath("wwwroot/" + newFile), FileMode.Create))
            {
                Image.CopyTo(st);
            }
            
            return newFile;
        }
    }
}