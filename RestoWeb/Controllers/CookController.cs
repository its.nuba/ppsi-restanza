using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace RestoWeb.Controllers
{
    using Helpers;

    [Authorize]
    public class CookController : BaseController
    {
        private readonly WebAPIConnector _api;

        public CookController(WebAPIConnector api)
        {
            _api = api;
        }

        public IActionResult Index()
        {
            var response = _api.SendGetList<RestoAPI.Models.Ext.OrderDetailWithStatus>("order/allorderdetail");

            return View(response.data);
        }

        public bool UpdateStatus(int id, int detailid, RestoAPI.Models.DB.OrderDetailStatus.StatusType status)
        {
            var iduser = Convert.ToInt32(User.FindFirst(ClaimTypes.Sid).Value);

            var response = _api.SendPost<RestoAPI.Models.Ext.OrderDetailWithStatus>("order/" + id + "/Detail/Status", new { orderdetailid = detailid, status = status, createdby = iduser });

            if (!response.status)
            {
                TempData["msg-color"] = response.status ? "success" : "warning";
                TempData["msg"] = response.message;
            }

            if (response.status)
            {
                return true;
            }

            return false;
        }
    }
}