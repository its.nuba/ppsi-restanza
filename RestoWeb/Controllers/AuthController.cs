using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace RestoWeb.Controllers
{
    using Helpers;

    public class AuthController : Controller
    {
        private readonly WebAPIConnector _api;

        public AuthController(WebAPIConnector api)
        {
            _api = api;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Login");
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(string username, string password, string returnUrl)
        {
            var param = new { username = username, password = password };
            var response = _api.SendPost<RestoAPI.Models.DB.User>("login", param);
            if (response.status)
            {
                var claim = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, response.data.Name),
                    new Claim(ClaimTypes.Sid, response.data.Id.ToString()),
                    new Claim(ClaimTypes.Role, response.data.RoleId.ToString())
                };

                var claimsIdentity = new ClaimsIdentity(claim, response.data.Password);
                var claimsPrinciple = new ClaimsPrincipal(claimsIdentity);

                await HttpContext.Authentication.SignInAsync("Cookies", claimsPrinciple);

                if (Url.IsLocalUrl(returnUrl)) return Redirect(returnUrl);

                return Redirect("~/");
            }
            else return RedirectToAction("Login", new { msg = response.message });
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");

            return Redirect("~/");
        }
    }
}