﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestoWeb.Models
{
    public class APIResult<T>
    {
        public APIResult(bool status, string message)
        {
            this.status = status;
            this.message = message;
        }

        public bool status { get; set; }
        public string message { get; set; }
        public T data { get; set; }
    }
}
